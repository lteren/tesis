import kodkod.ast.Decls;
import kodkod.ast.Relation;
import kodkod.ast.Variable;

import java.util.ArrayList;

/**
 * Created by leo on 9/5/17.
 */
public class Declarations {

    public static Decls getRealDeclarations(ArrayList<Variable> vars){
        Relation real = Relation.unary("Real");
        Decls result = vars.get(0).oneOf(real);
        for(Integer i = 1; vars.size() < i; i++){
            result.and(vars.get(i).oneOf(real));
        }

        return  result;
    }

}
