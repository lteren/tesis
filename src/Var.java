import kodkod.ast.Variable;

import java.util.HashMap;

/**
 * Created by leo on 2/5/17.
 */
public class Var {
    //como puedo hacer referencia a variables que ya existen, las voy guardando en este mapa para usar la misma y no
    //crear dos veces lo mismo
    public static HashMap<String, Variable> variableMap = new HashMap<>();

    public static Variable getVariable(String value){
        if(!variableMap.containsKey(value)){
            variableMap.put(value, Variable.unary(value));
        }
        return variableMap.get(value);
    }

}
