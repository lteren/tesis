import kodkod.ast.Formula;
import kodkod.ast.IntExpression;
import kodkod.ast.Relation;
import kodkod.ast.Variable;
import kodkod.ast.operator.IntCompOperator;
import kodkod.ast.operator.IntOperator;
import kodkod.ast.operator.Quantifier;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;

import static kodkod.ast.Variable.unary;

public class ReadXML{

    private  static Boolean firstTime = true;

    public static void main(String[] args) {

        try {

            File file = new File("teorías/test1.xml");

            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document doc = dBuilder.parse(file);

            /*System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            if (doc.hasChildNodes()) {

                printNote(doc.getChildNodes());

            }*/
            Formula finalFormula = createFormula(doc.getChildNodes());

            System.out.println(finalFormula.toString());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static Formula createFormula(NodeList nodeList){
        Formula result = null;
        for (int count = 0; count < nodeList.getLength(); count++){
            Node tempNode = nodeList.item(count);

            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                String  nodeName = tempNode.getNodeName();
                String textContent = tempNode.getTextContent();

                if(nodeName.equals("form") && firstTime){//si es el primer nodo
                    firstTime = false;
                    result = createFormula(tempNode.getChildNodes());
                }else if (nodeName.equals("form") && !firstTime){
                    return  createFormula(tempNode.getChildNodes());
                }else if (nodeName.equals("op")) {
                    if(textContent.equals("ALL") && Quantifier.valueOf(textContent )== Quantifier.ALL){

                        //tengo que hacer lo del bindeo
                        ArrayList<Variable> vars = new ArrayList<Variable>();
                        Node currentNode = tempNode;
                        while (currentNode.getNextSibling().getNodeName() == "bind"){
                            currentNode = currentNode.getNextSibling();
                            vars.add(Var.getVariable(currentNode.getTextContent()));
                        }

                        return createFormula(currentNode.getNextSibling().getChildNodes()).forAll(Declarations.getRealDeclarations(vars));
                    }if(textContent.equals("SOME") && Quantifier.valueOf(textContent )== Quantifier.SOME){
                        Variable x = Var.getVariable(textContent);
                        Relation real = Relation.unary("Real");
                        Node nextSibling = tempNode.getNextSibling();
                        Node nextNextSibling = nextSibling.getNextSibling();
                        return createFormula(nextNextSibling.getChildNodes()).forSome(x.oneOf(real));
                    }else if(textContent.equals("<=")) {
                        Node nextSibling = tempNode.getNextSibling();
                        Node nextNextSibling = nextSibling.getNextSibling();
                        IntExpression intExpression1 = createExpression(nextSibling.getFirstChild());
                        IntExpression intExpression2 = createExpression(nextNextSibling.getFirstChild());
                        return intExpression1.lte(intExpression2);
                        //chequeo si es un operador de fórmula
                    }else if(textContent.equals("EQ")){
                            Node nextSibling = tempNode.getNextSibling();
                            Node nextNextSibling = nextSibling.getNextSibling();
                            IntExpression intExpression1 = createExpression(nextSibling.getFirstChild());
                            IntExpression intExpression2 = createExpression(nextNextSibling.getFirstChild());
                            return intExpression1.compare(IntCompOperator.valueOf(textContent), intExpression2);
                            //chequeo si es un operador de fórmula

                    }else if(IntOp.getOperators().contains(textContent)){
                        return Form.getFormula(tempNode);
                    }else if(FormOp.getOperators().contains(textContent)){
                        return Form.getFormula(tempNode);
                    }
                }else{
                    System.out.println(nodeName);
                    System.out.println(textContent);
                }
            }

        }
        return result;
    }

    private  static IntExpression createExpression(Node nodeExpression){
        IntExpression result = null;
        String operator = getOperator(nodeExpression.getFirstChild());
        Node firstChild = nodeExpression.getFirstChild();
        Node nextSiblingOfFirstChild = firstChild.getNextSibling();

        //quiero ver si es un caso base o si tengo que seguir bajando recursivamente hasta obtener una variable o una constante
        Boolean itsVariableOrConstant = checkRecursion(nextSiblingOfFirstChild.getFirstChild());
        String arg1 = getArg(nextSiblingOfFirstChild);
        //if(operator.equals("*")){
        if(IntOperator.valueOf(operator) == IntOperator.MULTIPLY){
            Variable x = unary(arg1);
            //result = IntExpression.multiply(, null);
        }
        return result;
    }

    //este método te dice si hay que seguir bajando en la recursión o si llegamos a una variable o constante
    private static Boolean checkRecursion(Node node){
        return !node.hasChildNodes();
    }

    private static String getArg(Node node){
        String result = node.getTextContent();
        return result;
    }

    private static String getOperator(Node node){
        String result = node.getFirstChild().getTextContent();
        return  result;
    }

    private static void printNote(NodeList nodeList) {

        for (int count = 0; count < nodeList.getLength(); count++) {

            Node tempNode = nodeList.item(count);

            // make sure it's element node.
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

                // get node name and value
                System.out.println("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
                System.out.println("Node Value =" + tempNode.getTextContent());

                if (tempNode.hasAttributes()) {

                    // get attributes names and values
                    NamedNodeMap nodeMap = tempNode.getAttributes();

                    for (int i = 0; i < nodeMap.getLength(); i++) {

                        Node node = nodeMap.item(i);
                        System.out.println("attr name : " + node.getNodeName());
                        System.out.println("attr value : " + node.getNodeValue());

                    }

                }

                if (tempNode.hasChildNodes()) {

                    // loop again if has child nodes
                    printNote(tempNode.getChildNodes());

                }

                System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");

            }

        }

    }

}