import kodkod.ast.operator.FormulaOperator;

import java.util.HashSet;

/**
 * Created by leo on 2/5/17.
 */
public class FormOp {

    private static HashSet<String> operators;

    public static FormulaOperator getOperator(String value){
        return FormulaOperator.valueOf(value);
    }

    public static HashSet<String> getOperators(){
        if(operators == null){
            operators = new HashSet<String>();
            operators.add("AND");
            operators.add("IFF");
            operators.add("IMPLIES");
            operators.add("OR");
        }
        return operators;
    }

}
