import kodkod.ast.operator.IntOperator;

import java.util.HashSet;

/**
 * Created by leo on 9/5/17.
 */
public class IntOp {
    private static HashSet<String> operators;

    public static IntOperator getOperator(String value){
        return IntOperator.valueOf(value);
    }

    public static HashSet<String> getOperators(){
        if(operators == null){
            operators = new HashSet<String>();
            operators.add("ABS");
            operators.add("AND");
            operators.add("DIVIDE");
            operators.add("MINUS");
            operators.add("MODULO");
            operators.add("MULTIPLY");
            operators.add("NEG");
            operators.add("NOT");
            operators.add("OR");
            operators.add("PLUS");
            operators.add("SGN");
            operators.add("SHA");
            operators.add("SHL");
            operators.add("SHR");
            operators.add("XOR");
        }
        return operators;
    }
}
