import kodkod.ast.Formula;
import kodkod.ast.Variable;
import org.w3c.dom.Node;

/**
 * Created by leo on 2/5/17.
 */
public class Form {

    public static Formula getFormula(Node tempNode){
        String opName = tempNode.getTextContent();

        //chequeo si es una variable o si tengo una fórmula
        Node nextSibling = tempNode.getNextSibling();
        Node nextNextSibling = nextSibling.getNextSibling();

        Formula form1;
        Formula form2;

        if(!nextSibling.getFirstChild().hasChildNodes()){
            Variable v = Var.getVariable(nextSibling.getFirstChild().getTextContent());
            form1 = v.one();
        }else{
            form1 = ReadXML.createFormula(nextSibling.getChildNodes());
        }

        if(!nextNextSibling.getFirstChild().hasChildNodes()){
            Variable v = Var.getVariable(nextNextSibling.getFirstChild().getTextContent());
            form2 = v.one();
        }else{
            form2 = ReadXML.createFormula(nextNextSibling.getChildNodes());
        }
        return Formula.compose(FormOp.getOperator(opName), form1, form2);

    }

}
