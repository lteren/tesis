#!/bin/bash

# Author: Mariano Moscato (mariano.moscato@nianet.org)
# Last modification: 2021-01-30
#
# Description: Script to run kodkodi.

usage() {
    echo "run-kodkod -- runs kodkodi.

Usage: run-kodkodi [OPTIONS] 

  -h --help               Displays this message.
  
"  
}

while [ $# -gt 0 ]
do
  case $1 in
      -h|-help|--help)     
	  usage
	  exit 0;;
      -H| --kodkodi-home)
      	  KODKODI_HOME=$2;;
      -i| --input-file)
	  INPUT=$2;;
      -o| --output-file)
	  OUTPUT=$2;;
      -e| --error-file)
	  ERROR=$2;;
  esac
  if [ "$#" -gt 0 ]; then shift; fi
done

if [  -z $KODKODI_HOME ]; then
    KODKODI_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
fi

export CLASSPATH=$KODKODI_HOME/jar/antlr-runtime-3.1.1.jar:$KODKODI_HOME/jar/kodkod-1.5.jar:$KODKODI_HOME/jar/kodkodi-1.5.2.jar:$KODKODI_HOME/jar/sat4j-2.3.jar:
export JAVA_LIBRARY_PATH=$KODKODI_HOME/jni/x86-darwin:jni/x86-darwin
export LD_LIBRARY_PATH=$KODKODI_HOME/jni/x86-darwin:


if [ -z $INPUT ]; then
    if [ -z $OUTPUT ]; then
	if [ -z $ERROR ]; then
	    java $KODKODI_JAVA_OPT de.tum.in.isabelle.Kodkodi.Kodkodi
	else
	    java $KODKODI_JAVA_OPT de.tum.in.isabelle.Kodkodi.Kodkodi 2> $ERROR
	fi
    else
	if [ -z $ERROR ];then
	    java $KODKODI_JAVA_OPT de.tum.in.isabelle.Kodkodi.Kodkodi > $OUTPUT
	else
	    java $KODKODI_JAVA_OPT de.tum.in.isabelle.Kodkodi.Kodkodi > $OUTPUT 2> $ERROR
	fi
    fi
else
    if [ -z $OUTPUT ];then
	if [ -z $ERROR ];then
	    java $KODKODI_JAVA_OPT de.tum.in.isabelle.Kodkodi.Kodkodi < $INPUT 
	else
	    java $KODKODI_JAVA_OPT de.tum.in.isabelle.Kodkodi.Kodkodi < $INPUT 2> $ERROR
	fi
    else
	if [ -z $ERROR ];then
	    java $KODKODI_JAVA_OPT de.tum.in.isabelle.Kodkodi.Kodkodi < $INPUT > $OUTPUT
	else
	    java $KODKODI_JAVA_OPT de.tum.in.isabelle.Kodkodi.Kodkodi < $INPUT > $OUTPUT 2> $ERROR
	fi
    fi
fi	
	

