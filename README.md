# Installation notes

* the local folder containing this repository has to be included in the `PVS_LIBRARY_PATH` environment variable, so PVS can load the patches where the strategies are implemented.

* Additoinally, the folder `kodkodi-1.5.2` has to be included in the `PATH` of the system
