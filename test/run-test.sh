#!/bin/sh

if [ -f $1 ]; then
    PVSFILENAME=$1
else
    echo "Missing input file"
    echo "Usage: run-test.sh <pvs filaname>"
    exit 1
fi
    
pvs -raw -E "(load \"test-find-model.lisp\")(analyze-formulas-in-file \"$PVSFILENAME\")(bye 0)"
