;;

(defvar *model-finder-validation* t "When t, proof command inputs are validated via model finding.")

;;;
;;; State defining globale variables
;;;

(defvar *atom-index*)
(defvar *kki-spec-formulas-count*)
(defvar *var-count*)
(defvar *used-types*)
(defvar *pvs-top-types* nil "Names (strings) of the PVS super-types referred to in the active formulas.")
(defvar *free-var-types*)

(defvar *unary-relation-count*  -1 "Relation name index counter for sets.")
(defvar *binary-relation-count* -1 "Relation name index counter for binary relations.")
(defvar *nary-relation-count*   -1 "Relation name index counter for relations of arity 3 or more.")

(defvar *used-relations*)
(defvar *back-translation-data* nil)
(defvar *kki-type-atom-map*)
(defvar *subtypes-with-top-types*)
(defvar *funtype-type-names-in-cartesian-product* nil "kki set names corresponding to each type-name in the definition of a PVS function")
(defvar *used-adts* nil "PVS ADTs used in the formulas to be analyzed.")

(defvar *tuple-set-reg-counter* -1)
(defvar *kki-spec-bounds*   nil "Kodkodi specification part: bounds")
(defvar *kki-spec-formulas* nil "Kodkodi specification part: formulas")
(defvar *kki-spec-tuple-reg-directives* nil "Kodkodi specification part: tuple register directives")

(defvar *exact-bounds?* nil)

(defvar *last-instance* nil)

(defvar *overriden-axioms* nil "List of PVS axioms overriden by ad-hoc formulae more amenable to kodkod")

;;;
;;;
;;;

(defun analyze-subgoals (current-goal subgoals)
  "Returns an error message if the analysis failed, nil otherwise." ;; deprecated
  ;; For each subgoal SG,
  ;; - if it has more consequents than CURRENT-GOAL, the validity of the whole SG
  ;;   is checked by trying to construct a counterexample.
  ;; - otherwise, if it has more antecedents than CURRENT-GOAL, their consistency
  ;;   is checked by trying to construct a model for the antecedents in SB.
  ;; The arguments are expected to be of the following form:
  ;; * CURRENT-GOAL: [ \Gamma |- \Delta ]
  ;; * SUBGOALS: ( [ \alpha_1, ..., \alpha_n, \Gamma |- \Delta ] ,
  ;;               [ \alpha_2, ..., \alpha_n, \Gamma |- \alpha_1, \Delta ] ,
  ;;               ...
  ;;               [ \alpha_n, \Gamma |- \alpha_1, ..., \alpha_{n-1}, \Delta ] ,
  ;;               [ \Gamma |- \alpha_1, ..., \alpha_n, \Delta ] )
  (when subgoals
    ;; (let ((current-goal-consequents-count (length (p-sforms current-goal)))
    ;; 	  (current-goal-antecedents-count (length (n-sforms current-goal))))
    ;;   (loop with analysis-result=nil
    ;; 	    for subgoal in (cdr subgoals)
    ;; 	    do (setf analysis-result (find-model-for-forms (loop for sf in (s-forms subgoal) collect (formula sf)) :counter-example? t))
    ;; 	    when analysis-result
    ;; 	    do (return analysis-result)))
    
    ;; look for a model for (the antecedents) the first subgoal
    (let*((first-subgoal (car subgoals))
	  (ants-s-forms (neg-s-forms* first-subgoal))
	  ;;(s-formula? )
	  (antecedents (loop for sf in ants-s-forms when (s-formula? sf) collect (args1(formula sf))))
	  (analysis-results (list (find-model-for-forms
				   antecedents
				   :counter-example? nil))))
      (loop for subgoal in (cdr subgoals)
	    do (push (find-model-for-forms (loop for sf in (s-forms subgoal) collect (formula sf))
					   :counter-example? t)
		     analysis-results)
	    when (car analysis-results) ;; panic mode, return at first counterexample found
	    do (return analysis-results))
      (reverse analysis-results))
    ;; (if (find-model-for-forms (loop for sf in (n-sforms (car subgoals)) collect (args1(formula sf)))
    ;; 			      :counter-example? nil)
    ;; 	(loop with analysis-result=nil
    ;; 	      for subgoal in (cdr subgoals)
    ;; 	      do (setf analysis-result
    ;; 		       (find-model-for-forms (loop for sf in (s-forms subgoal) collect (formula sf))
    ;; 					     :counter-example? t))
    ;; 	      when analysis-result
    ;; 	      do (return analysis-result))
    ;;   (format nil
    ;; 	      "Model for the antecedents in the sequent~% ~a~%cannot be found. The sequent might be inconsistent.~%"
    ;; 	      (car subgoals)))
    ))

;;; poner el handler case en la llamada a find-model

(defun find-model-for-forms
    (forms
     &key
     (univ-size 10)       ;; universe size, useful for uninterpreted atoms
     (exact-bounds? nil)  ;; if true, the model will use the exact bounds given
     (verbose? nil)       ;; provides all the info returned by Kodkodi
     (model-as-pvs? nil)  ;; displays the model that was found in PVS grammar
     (counter-example? t) ;; if true, we will try to find a counter-example
     (dry-run? nil))      ;; if nil, no info will be display, just if a model was or wasn't found
  "Tries to build a model for the formulas in FNUMS. If COUNTER-EXAMPLE? ~
   is t (default), the returned model falsifies the formulas; otherwise, ~
   it fulfills them.
   UNIV-SIZE: upper bound on the size of the whole universe for the model. ~
   EXACT-BOUNDS?: if true, UNIV-SIZE-1 is also a lower bound. ~
   VERBOSE?: when t, extra information is presented to the user. ~
   MODEL-AS-PVS?: shows the model (if found) in PVS language. ~
   DRY-RUN?: when t, the formulas are translated to the language accepted ~
   by the backend but no actual command is sent to it."
  (let ((*exact-bounds?* exact-bounds?)
	(kki-spec (generate-kodkod-spec forms univ-size exact-bounds? verbose? counter-example?)))
    (when kki-spec
      (let ((dummy (when (and (numberp verbose?) (> verbose? 0))
		     (format t "Kodkodi specification: ~%")
		     (format t "~{ ~a ~%~}" kki-spec))))
	(if dry-run?
	    (format t "Dry-run  mode.")
	  (let*((dummy (when verbose? (format t "Running Kodkod...~%")))
		(translation-result (kki-analyze kki-spec))
		(dummy (when verbose? (format t "~%~%~a~%~%" translation-result))))
	    (if (>= 1 (car translation-result))
		(cond ((search "SATISFIABLE" (rest translation-result))
		       (if verbose?
			   (let*((start (+ (search "---INSTANCE---"
						   (rest translation-result)) 27))
				 (end (search "---STATS---" (rest translation-result)))
				 (dummy (setf *last-instance* (subseq (rest translation-result) start end))))
			     (format nil "~a found:~%~a~%"
				     (if counter-example? "Counterexample" "Instance")
				     (if model-as-pvs?
					 (parse-kki-instance *last-instance*)
				       (translate-instance *last-instance*))))
			 (format nil "~a found" (if counter-example? "Counterexample" "Instance")))))
	      (error (format nil "Kodkod Error: ~a" (cdr translation-result))))))))))

;;;
;;; Strategies
;;;

(defstep inst?? (&optional
		     (fnums 1)
		     (univ-size 10)       ;; universe size, useful for uninterpreted atoms
		     (exact-bounds? nil)  ;; if true, the model will use the exact bounds given
		     (verbose? nil)       ;; provides all the info returned by Kodkodi
		     (model-as-pvs? nil)  ;; displays the model that was found in PVS grammar
		     (dry-run? nil))      ;; if nil, no info will be displayed, just if a model was or wasn't found
  (let ((*exact-bounds?* exact-bounds?)
	(seq-form (car(extra-get-seqfs fnums))) ;;TODO check there's only one
	(form (if (and (negation?(formula seq-form)) (forall?(argument(formula seq-form))))
		  (expression(argument(formula seq-form)))
		(if (exists?(formula seq-form))
		    (expression(formula seq-form))
		  (error "form is not existential"))))
	(bindings (if (and (negation?(formula seq-form)) (forall?(argument(formula seq-form))))
		      (bindings(argument(formula seq-form)))
		    (bindings(formula seq-form))))
	(kki-spec (handler-case
		      (generate-kodkod-spec (list form) univ-size exact-bounds? verbose? nil bindings)
		    (unsopported-feature-error (ex) (format t "~%Translation error: ~a~%" (text ex))))))
    (when kki-spec ;;TODO error message when kki-spec was not generated
      (let ((dummy (when (and (numberp verbose?) (> verbose? 0))
		     (format t "Kodkodi specification: ~%")
		     (format t "~{ ~a ~%~}" kki-spec))))
	(if dry-run?
	    (skip-msg "Dry-run  mode." )
	  (let((dummy (when verbose? (format t "Running Kodkod...~%")))
	       (translation-result (kki-analyze kki-spec))
	       (dummy (when verbose? (format t "~%~%~a~%~%" translation-result))))
	    (if (and (>= 1 (car translation-result)) (search "SATISFIABLE" (rest translation-result)))
		(let ((start (+ (search "---INSTANCE---" (rest translation-result)) 27))
		      (end (search "---STATS---" (rest translation-result)))
		      (last-instance (subseq (rest translation-result) start end))
		      (msg (format nil "~%~{~a~%~}" (parse-kki-instance last-instance))))
		  (skip-msg msg))
	      (let ((error-msg "no-instance-found")) ;;TODO error handling
		(skip-msg error-msg))))))))
  "" "")

(defstep find-model (&optional
		     (fnums 1)
		     (univ-size 10)       ;; universe size, useful for uninterpreted atoms
		     (exact-bounds? nil)  ;; if true, the model will use the exact bounds given
		     (verbose? nil)       ;; provides all the info returned by Kodkodi
		     (model-as-pvs? nil)  ;; displays the model that was found in PVS grammar
		     (counter-example? t) ;; if true, we will try to find a counter-example
		     (dry-run? nil))      ;; if nil, no info will be displayed, just if a model was or wasn't found
  (let ((dummy (format t "~&Searching for ~:[instance~;counterexample~]...~%" counter-example?))
	(forms (loop for sf in (extra-get-seqfs fnums) collect (formula sf)))
	(kki-spec (handler-case
		      (generate-kodkod-spec forms univ-size exact-bounds? verbose? counter-example?)
		    (unsopported-feature-error (ex) (format t "~%Translation error: ~a~%" (text ex))))))
    (when kki-spec ;;TODO error message when kki-spec was not generated
      (let ((dummy (when (and (numberp verbose?) (> verbose? 0))
		     (format t "Kodkodi specification: ~%")
		     (format t "~{ ~a ~%~}" kki-spec))))
	(if dry-run?
	    (skip-msg "Dry-run  mode." )
	  (let((dummy (when verbose? (format t "Running Kodkod...~%")))
	       (translation-result (kki-analyze kki-spec))
	       (dummy (when verbose? (format t "~%~%~a~%~%" translation-result)))
	       (str (if (>= 1 (car translation-result))
			(cond 
			 ((search "UNSATISFIABLE" (rest translation-result))
			  (if verbose?
			      (format nil "~a not found.~%" (if counter-example? "Counterexample" "Instance"))
			    (format nil "~a not found" (if counter-example? "Counterexample" "Instance"))))
			 ((search "SATISFIABLE" (rest translation-result))
			  (if verbose?
			      (let*((start (+ (search "---INSTANCE---" (rest translation-result)) 27))
				    (end (search "---STATS---" (rest translation-result)))
				    (dummy (setf *last-instance* (subseq (rest translation-result) start end))))
				(format nil "~a found:~% ~a~%"
					(if counter-example? "Counterexample" "Instance")
					(if model-as-pvs?
					    (format nil "~%~{~a~%~}" (parse-kki-instance *last-instance*))
					  (translate-instance *last-instance*))))
			    (format nil "~a found." (if counter-example? "Counterexample" "Instance")))))
		      (format nil "Kodkod Error: ~a" (cdr translation-result)))))
	    (skip-msg str))))))
  "Tries to build a model for the formulas in FNUMS. If COUNTER-EXAMPLE? ~
   is t (default), the returned model falsifies the formulas; otherwise, ~
   it fulfills them.
   UNIV-SIZE: upper bound on the size of the whole universe for the model. ~
   EXACT-BOUNDS?: if true, UNIV-SIZE-1 is also a lower bound. ~
   VERBOSE?: when t, extra information is presented to the user. ~
   MODEL-AS-PVS?: shows the model (if found) in PVS language. ~
   DRY-RUN?: when t, the formulas are translated to the language accepted ~
   by the backend but no actual command is sent to it."
  "Finding a ~:[counter-example~;model~] for fnum~[~;s~] ~{~a~^, ~}...")

(defun kki-setup ()
  (setq *last-instance* nil)
  (setq *atom-index* 0)
  (setq *var-count* -1)
  (setq *kki-spec-formulas-count* -1)
  (setq *unary-relation-count* -1)
  (setq *binary-relation-count* -1)
  (setq *nary-relation-count* -1)
  (setq *used-types* nil)
  (setq *pvs-top-types* nil)
  (setq *used-relations* nil)
  (setq *back-translation-data* nil)
  (setq *free-var-types* nil)
  (setq *kki-type-atom-map* nil)
  (setq *subtypes-with-top-types* nil)
  (setq *funtype-type-names-in-cartesian-product* nil)
  (setq *used-adts* nil)
  
  (setq *tuple-set-reg-counter* -1)
  (setq *kki-spec-bounds* nil)
  (setq *kki-spec-formulas* nil)
  (setq *kki-spec-tuple-reg-directives* nil)
  (setq *exact-bounds?* nil)
  (setq *overriden-axioms* nil))

(defun kki-analyze (kki-spec)
  (with-open-file (stream (ensure-directories-exist "ToProve.kki") 
			  :direction :output
			  :if-exists :supersede)
		  (format stream "~{~a ~%~}" kki-spec))
  (let* ((kki-call "bash run-kodkod.sh -i ToProve.kki")
	 (result (extra-system-call kki-call)))
    result))

(defun build-expressions-existence-kki-formula (fmla)
  "Generates a list of Kodkod f(type term)ormulas to force existence of the expression appearing in the FMLA formula."
  #+pvsdebug (format t "~%[build-expressions-existence-kki-formula] fmla ~a~%" fmla)
  (loop for term
	in (collect-subterms fmla
			     #'(lambda (e) (and (application? e)
						(not (is-bool-type (type e))))))
	#+pvsdebug do
	#+pvsdebug (progn
		     (format t "~&[build-expressions-existence-kki-formula] term ~a~%" term)
		     (format t "~&[build-expressions-existence-kki-formula] (free-variables term) ~a~%" (free-variables term))
		     (format t "~&[build-expressions-existence-kki-formula] (nonempty? (type term)) ~a~%" (nonempty? (type term))))
	unless (free-variables term)
	;; when (nonempty? (type term)) ;;TODO remove ;; this restriction of non-emptyness should be present when the type is proven to be not empty, but the nonempty? field is true when the type is known to be nonempty by the typechecker... the type could be non empty even if this field is nil.
	collect (format nil "~a /* non-emptyness of subexpression */ := some(~a)"
			(kki-form-name)
			(translate-to-kki term))))

(defun generate-kodkod-spec (forms
			    univ-size
			    exact-bounds?
			    &optional
			    (verbose? nil)
			    (find-counterexample? t)
			    bindings
			    (theory (theory (context *ps*))))
  ;; Global variables are reseted each time the strategy is invoked
  (kki-setup)
  #+pvsdebug (format t "~%[generate-kodkod-spec] exact-bounds? ~a~%" exact-bounds?)
  (let* ((antecedents (loop for fmla in forms when (negation? fmla) collect fmla))
	 (*exact-bounds?* exact-bounds?)
	 #+pvsdebug
	 (dummy (format t "~%[generate-kodkod-spec] antecedents ~{~a ~}~%" antecedents))
	 (consequents (loop for fmla in forms unless (negation? fmla) collect fmla))
	 #+pvsdebug
	 (dummy (format t "~&[generate-kodkod-spec] consequents ~{~a ~}~%" consequents))
	 (skolem-constants
	  (let ((skcts nil))
	    (loop for formula in forms
		  do (setq
		      skcts
		      (append
		       (loop for sk in (collect-subterms formula #'skolem-constant?)
			     unless (member sk skcts)
			     collect sk)
		       skcts)))
	    skcts))
	 #+pvsdebug
	 (dummy (format t "~&[generate-kodkod-spec] skolem-constants ~a~%" skolem-constants))
	 (fresh-kki-names-for-skolem-constants
	  ;; (when skolem-constants (kki-id-names (length skolem-constants)))
	  (loop for free-var in skolem-constants
		collect (kki-name-for-name-expr free-var))))
    ;; Process bindings
    (when bindings
      (loop for binding in bindings do (kki-name-for-name-expr binding)))
    ;; Process Skolem constants
    (when skolem-constants
      (when verbose?
	(format t "Substitutions: ~{~a -> ~a~^, ~}~%"
		(merge-lists skolem-constants fresh-kki-names-for-skolem-constants)))
      (loop for sk in skolem-constants
	    when (adt-type-name? (type sk))
	    do (process-adt (type sk))))
    ;; Process formulas from the sequent
    (let*((skolem-constants-substitution
	   (pairlis (loop for fv in skolem-constants
			  collect (string(id fv)))
		    fresh-kki-names-for-skolem-constants))
	  #+pvsdebug
	  (dummy (format t "~&[generate-kodkod-spec] skolem-constants-substitution ~a~%"
			 skolem-constants-substitution)))
      (setq *free-var-types*
	    (loop for (pvs-sk-cnt-str . kki-name) in skolem-constants-substitution
		  for pvs-sk-cnt in skolem-constants
		  collect (cons (intern kki-name) (type pvs-sk-cnt))))
      (let ((sequent-formulas (if find-counterexample?
	       ;; if searching for counterexample, 
	       (append
		(loop for fmla in antecedents
		      ;; collect (format nil "~a := ~{some(~a) && ~}(~a) "
		      ;; 		      (kki-form-name)
		      ;; 		      (build-expressions-existence-kki-formula fmla)
		      ;; 		      (translate-to-kki (argument fmla) skolem-constants-substitution))
		      collect (format nil "~a := ~a "
		      		      (kki-form-name)
		      		      (translate-to-kki (argument fmla) skolem-constants-substitution))
		      append (build-expressions-existence-kki-formula fmla)
		      )
		;; consequents in positive
		(loop for fmla in consequents
		      ;; collect (format nil "~a := !(~{some(~a) && ~}(~a)) "
		      ;; 		      (kki-form-name)
		      ;; 		      (build-expressions-existence-kki-formula fmla)
		      ;; 		      (translate-to-kki fmla skolem-constants-substitution))
		      collect (format nil "~a := !(~a) "
		      		      (kki-form-name)
		      		      (translate-to-kki fmla skolem-constants-substitution))
		      append (build-expressions-existence-kki-formula fmla)
		      ))
	     ;; if searching for a model, state the underlying implication of
	     ;; f-forms: !(&&_1^n(antecedents)) || ||_1^m(consequents)
	     (let ((kki-disjuncts
		    (append
		     (loop for fmla in consequents
			   collect (translate-to-kki fmla skolem-constants-substitution))
		     (loop for fmla in antecedents
			   collect (translate-to-kki fmla skolem-constants-substitution)))))
	       (append (list (format nil "~:[~;~a := ~{~a~^ || ~}~]"
				     kki-disjuncts
				     (kki-form-name)
				     kki-disjuncts))
		       (loop for fmla in antecedents
			     append (build-expressions-existence-kki-formula fmla)
			     ;; append (loop for expr in (build-expressions-existence-kki-formula fmla)
			     ;; 		  collect (format nil "~a /* Expression from previous formula */ := some(~a)"
			     ;; 				  (kki-form-name)
			     ;; 				  (translate-to-kki term)))
			     )
		       (loop for fmla in consequents
			     append (build-expressions-existence-kki-formula fmla)
			     ;; append (loop for expr in (build-expressions-existence-kki-formula fmla)
			     ;; 		  collect (format nil "~a /* Expression from previous formula */ := some(~a)"
			     ;; 				  (kki-form-name)
			     ;; 				  (translate-to-kki term)))
			     )))))
	    (implicit-sequent-formulas
	     ;; If the formula has free variables, additional formulae are
	     ;; needed to define them (ie $f7 := s2 in s0 && one s2)
	     (loop for (free-var-kki-name . free-var-type) in *free-var-types*
		 collect (let ((kki-name (get-kki-name-for-pvs-type free-var-type)))
			   (format nil "~a := ~a in ~a && one ~a" ;;TODO check is this 'one' assuming the vars are first order? 
				   (kki-form-name)
				   free-var-kki-name
				   kki-name
				   free-var-kki-name)))))
	#+pvsdebug (format t "~&[generate-kodkod-spec] sequent-formulas ~a~%" sequent-formulas)
	#+pvsdebug (format t "~&[generate-kodkod-spec] implicit-sequent-formulas ~a~%" implicit-sequent-formulas)
	#+pvsdebug (break)
	(setf *kki-spec-formulas* (append sequent-formulas implicit-sequent-formulas *kki-spec-formulas*))))
    ;; Axioms
    (setf
     *kki-spec-formulas*
     (append
      (loop for (kkaxiom . pvsaxiom) in (translate-axioms theory)
	    if kkaxiom
	    collect (format nil "~a /* axiom ~a */ := ~a" (kki-form-name) pvsaxiom kkaxiom)
	    else
	    collect (format nil "/* Omitting axiom ~a */" pvsaxiom))
      *kki-spec-formulas*))
    ;; #+pvsdebug
    ;; (format t "~&[generate-kodkod-spec] *kki-spec-formulas* ~a ~%" *kki-spec-formulas*)
    ;; Kodkodi relations representing PVS functions are forced to be functional
    (setf
     *kki-spec-formulas*
     (append
      (loop for (op-kki-id . type-names)
	    in *funtype-type-names-in-cartesian-product*
	    collect (format nil "~a := FUNCTION(~a, ~{~a~^ ->lone ~})"
			    (kki-form-name)
			    op-kki-id
			    (loop for type-name in type-names
				  collect (get-kki-name-for-pvs-type type-name))))
      *kki-spec-formulas*))
;;    #+pvsdebug (format t "~&[generate-kodkod-spec] *kki-spec-formulas* ~a ~%" *kki-spec-formulas*)
    ;; Tuple set assignments
    (setf *kki-spec-tuple-reg-directives*
	  (append
	   (let ((top-set-size    (get-atoms-qty univ-size)))
	     (loop for i from 0 to *tuple-set-reg-counter*
		   collect
		   (format nil
			   "$a~a := u~a"
			   i
			   (if (= 0 i)
			       top-set-size
			     (format nil "~a@~a" (* top-set-size i) top-set-size)))))
	   *kki-spec-tuple-reg-directives*))
;;    #+pvsdebug (format t "~&[generate-kodkod-spec] *kki-spec-formulas* ~a ~%" *kki-spec-formulas*)
    ;; Formulas to represent the PVS type hierarchy
    (setf *kki-spec-formulas*
	  (append
	   (loop for (subtype . ttype) in (reverse *subtypes-with-top-types*)
		 collect (let ((subtype-kki-name
				(rest (assoc subtype *used-types* :test #'string=)))
			       (top-type-kki-name
				(rest (assoc ttype *used-types* :test #'string=))))
			   (format nil
				   "~a := ~a in ~a"
				   (kki-form-name)
				   subtype-kki-name
				   top-type-kki-name)))
	   
	   *kki-spec-formulas*))
    ;; Add axioms from other declarations
    (loop for decl
	  in (set-difference (all-declarations theory)
			     (all-decls-generated-by-adts theory))
	  when (and (def-decl? decl) (def-axiom decl))
	  do (handler-case
		 (let((definition-axiom (if (listp (def-axiom decl)) (car (def-axiom decl)) (def-axiom decl))))
		   (push (format nil
				 "~a /* definition axiom for declaration ~a */ := ~a"
				 (kki-form-name)
				 (id decl)
				 (translate-to-kki definition-axiom))
			 *kki-spec-formulas*))
	       (unsopported-feature-error
		(ex)
		(format t "~&Warning: omitting definition axiom for ~a (~a)~%" (id decl) (text ex)))))
    ;; Solve statement
    (let ((kki-solve-statment
	   (let ((form-names (get-forms-names)))
	     (list (format nil "solve $f0 ~:[~;&& ~]~{~a~^ && ~};" form-names form-names))))
	  ;; - Mapping between PVS and Kodkodi elements are printed as comments
	  ;;   to improve readability.
	  (comments
	   (list
	    (format nil "/* PVS Types: ~{~a~^, ~} */"
		    (loop for (a . b) in *used-types*
			  collect (format nil " ~a as ~a " a b)))
	    (format nil "/* PVS Skolem Constants: ~a */" *free-var-types*)
	    (format nil "/* PVS Functions: ~{~a~^, ~} */"
		    (loop for (a . b) in *used-relations*
			  collect (format nil "(~a: ~a)" a b)))))
	  ;; - Parameters of the model finder process.
	  (kki-options
	   (list "solver: \"MiniSat\""
		 "symmetry_breaking: 3"
		 "flatten: false"))
	  ;; Univ spec
	  (kki-univ-spec (list (format nil "univ : u~a" univ-size)))
	  ;; Bounds for subtypes
	  ;; #+pvsdebug (dummy (break "Bounds for subtypes"))
	  (*kki-spec-bounds*
	   (append
	    (loop for (subtype . ttype) in (reverse *subtypes-with-top-types*)
		  collect
		  (let* ((subtype-kki-name
			  (rest (assoc subtype *used-types* :test #'string=)))
			 (top-type-kki-name
			  (rest (assoc ttype *used-types* :test #'string=)))
			 (top-type-atoms (rest (assoc top-type-kki-name
						      *kki-type-atom-map*
						      :test #'string=))))
		    #+pvsdebug (format t "~%[generate-kodkod-spec] exact-bounds? ~a~%" exact-bounds?)
		    (format nil
			    "bounds ~a: [~a, ~a]"
			    subtype-kki-name
			    (if exact-bounds? top-type-atoms "{}")
			    top-type-atoms)
		    ;; (format nil
		    ;; 	    (if exact-bounds?
		    ;; 		"bounds ~a: ~a"
		    ;; 	      "bounds ~a: [{}, ~a]")
		    ;; 	    subtype-kki-name
		    ;; 	    top-type-atoms)
		    ))
	    *kki-spec-bounds*))
	 ;; Bounds for skolem constants
	  ;; (*kki-spec-bounds*
	  ;;  (append
	  ;;   (loop for (free-var-kki-name . free-var-type) in *free-var-types*
	  ;; 	  collect
	  ;; 	  (let*((kki-name
	  ;; 		 (rest (assoc (get-top-type-id free-var-type)
	  ;; 			      *used-types*
	  ;; 			      :test #'string=))))
	  ;; 	    (format nil (if exact-bounds?
	  ;; 			    "bounds ~a: ~a"
	  ;; 			  "bounds ~a: [{}, ~a]")
	  ;; 		    free-var-kki-name
	  ;; 		    (rest (assoc kki-name *kki-type-atom-map* :test #'string=)))))
	  ;;   *kki-spec-bounds*))
	 ;; Bounds for PVS functions
	 ;;TODO check if this is still needed
	  (*kki-spec-bounds*
	   (append
	    (loop for (op-kki-id . type-names)
		  in *funtype-type-names-in-cartesian-product*
		  collect (let((bound (get-arity-types op-kki-id)))
			    (format nil
				    "bounds ~a: [~a, ~a]"
				    op-kki-id
				    (if exact-bounds? bound "{}")
				    bound)
			    ;; (format nil (if exact-bounds?
			    ;; 		  "bounds ~a: ~a"
			    ;; 		"bounds ~a: [{}, ~a]")
			    ;; 	  op-kki-id
			    ;; 	  (get-arity-types op-kki-id))
			    ))
	    *kki-spec-bounds*)))
      ;; Merge all parts to build specifications
      (append comments
	      kki-options
	      kki-univ-spec
	      *kki-spec-tuple-reg-directives*
	      *kki-spec-bounds*
	      *kki-spec-formulas*
	      kki-solve-statment))))

(defun adt-kki-axioms (adt)
  (let*((recognizers (loop for recognizer in (recognizers adt)
			   collect recognizer))
	(rec-kki-names (loop for rec in recognizers
			     collect (kki-name-for-name-expr rec)))
	(constructors (loop for constructor in (constructors adt)
			    collect (cons constructor
					  (loop for accessor in (accessors constructor)
						collect accessor))))
	(begin-comment (format nil "/* BEGIN Axioms for ADT ~a */" (id adt)))
	(end-comment (format nil "/* END Axioms for ADT ~a */" (id adt)))
	(disjointness                   ;; Disjointness Axiom
					; pairwise disjointness
	 (when (> (length rec-kki-names) 0)
	   (format nil "~a /* Disjointness */ := ~{no (~{~a~^ & ~})~^ && ~}"
		   (kki-form-name)
		   (loop for kki-name in rec-kki-names
			 append (loop for kki-name2 in rec-kki-names
				      unless (string= kki-name kki-name2)
				      collect (list kki-name kki-name2))))))
	(exhaustiveness  ; Exhaustiveness Axiom (PVS list_inclusive axiom)
	 (when (> (length rec-kki-names) 0)
	   (format nil "~a /* Exhaustiveness */ := ~{~a~^ + ~} = ~a"
		   (kki-form-name)
		   rec-kki-names (get-kki-name-for-pvs-type adt))))
	(uniqueness (get-uniqueness-axioms constructors))
	(sel ;;; these axioms were stating that the accessors are total and
	     ;;; functional, and which their domain is. I think it's better if we
	     ;;; let Kodkod produce partial models (not assuring totality of accessors),
	     ;;; we are already saying that the accessors are functional in "Uniqueness" (?)
	     ;;; we are already stating their domain using domain restrictions (which makes
	     ;;; more sense).
	     ;;; (get-sel-axioms (get-kki-name-for-pvs-type adt) constructors recognizers)
	 nil ;; TODO remove "sel" binding
		    )
	(acycl      (get-acycl-axioms (get-type-id adt) constructors))
	(additional (get-additional-axioms adt constructors))
	(cons-rec   (get-constructor-recognizer-axioms (get-kki-name-for-pvs-type adt) constructors recognizers))
	(acc-dom    (get-accessor-domain-axioms constructors)))
    (append
     (list begin-comment disjointness exhaustiveness)
     ;; constructor domains
     ;; (loop for constructor in (constructors adt)
     ;; 	   collect (type constructor))
     ;;(list (format nil "~a /**/ := m3_0 in [s0 -> s3 -> s5]" (kki-form-name)))
     ;;(list (format nil "~a /**/ := all [m0: m3_0] | m0 in [s0 -> s3 -> s5] " (kki-form-name)))
    ;; (list (format nil "~a /**/ := s4 = s0 " (kki-form-name)))
     ;;
     uniqueness
     sel
     acycl
     additional
     cons-rec
     ;; TODO remove
     ;; cons domain
     ;; (loop for constructor in constructors
     ;;  	   collect
     ;; 	   (let ((fun (car constructor)))
     ;; 	     (get-function-type-axiom (id fun) (type fun))))
     acc-dom
     (list end-comment))))

(defmethod get-kki-names-for-pvs-type ((ftype funtype))
  (append
   (get-kki-names-for-pvs-type (domain ftype))
   (get-kki-names-for-pvs-type (range ftype))))

(defmethod get-kki-names-for-pvs-type ((stype subtype))
  (list(get-kki-name-for-pvs-type stype)))

(defmethod get-kki-names-for-pvs-type ((type type-name))
  (list(get-kki-name-for-pvs-type type)))

(defmethod get-kki-names-for-pvs-type ((ttype tupletype))
  (loop for type in (types ttype)
	append (get-kki-names-for-pvs-type type)))

(defun get-accessor-domain-axioms (constructors)
  (loop for ( constructor . accessors ) in constructors
	when accessors
	collect (format nil "~a /* Accessor Domain [constructor: ~a, accessors ~{~a~^ ~}] */ := ~{~a~^ && ~}"
			(kki-form-name)
			constructor
			accessors
			(get-accessor-domain-axiom constructor accessors))))

(defun get-accessor-domain-axiom (constructor accessors)
  (loop for accessor in accessors
	collect (format nil "(~a = ~a.univ ~:* && univ.~a in ~a)"
			(get-kki-name-for-pvs-type (domain (type accessor)))
			(kki-name-for-name-expr accessor)
			(get-kki-name-for-pvs-type (range (type accessor))))))

(defun get-constructor-recognizer-axioms (kki-set-for-adt constructors recognizers)
  (loop for ( constructor . accessors ) in constructors
	for recognizer in recognizers
	collect (format nil "~a /* Cons-Rec */ := ~a" (kki-form-name) (get-constructor-recognizer-axiom  kki-set-for-adt constructor accessors recognizer))))

(defun get-constructor-recognizer-axiom (kki-set-for-adt constructor accessors recognizer)
  (format nil "~a~:[~;(~]~{~a~^, ~}~:[~;)~] = ~a"
	  (kki-name-for-name-expr constructor)
	  accessors
	  (loop for accessor in accessors
		collect (get-kki-name-for-pvs-type (range (type accessor))))
	  accessors
	  (kki-name-for-name-expr recognizer)))

(defun get-additional-axioms (adt constructors)
  (loop for ( constructor . accessors ) in constructors
	when accessors
	collect (format nil "~a /* constructor-accessor relation */ := ~a"
			(kki-form-name)
			(get-additional-axiom adt constructor accessors))))

(defun get-additional-axiom (adt constructor accessors)
  (let*((kki-constructor-range (get-kki-name-for-pvs-type (range (type constructor))))
	(return-types (loop for accessor in accessors
			    collect (get-kki-name-for-pvs-type (range (type accessor)))))
	(kki-ops      (loop for accessor in accessors
			    collect (kki-name-for-name-expr accessor)))
	(vars         (loop for i from 0 to (- (length return-types) 1)
			    collect (format nil "S~a" i)))
	(bindings     (loop for kki-set-name in return-types
			    for var in vars
			    collect (format nil "~a: one ~a" var kki-set-name)))
	(inner-expr   (format nil "(S~a in ~a(~{~a~^, ~}) <=> (~{~a~^ && ~}) )"
			      ;; TODO => OR <=> ?
			      (length return-types)
			      (kki-name-for-name-expr constructor)
			      vars
			      (loop for kki-op in kki-ops
				    for var in vars
				    collect (format nil "~a in ~a(S~a)" var kki-op (length return-types)))))) 
    ;; The formula returned by this function rephrases a PVS axiom 
    ;; that's contradictory with the finiteness of the kodkod models.
    (loop for accessor in accessors
	  do (push (intern (format nil "~a_~a_~a" adt accessor constructor)) *overriden-axioms*))
    (format nil "all[~{~a, ~}S~a: one ~a] | ~a " bindings (length return-types) kki-constructor-range inner-expr )))


(defun get-acycl-axioms (pvs-type-name constructors)
  (let ((acycl-accessors (loop for ( constructor . accessors ) in constructors
			       append (loop for accessor in accessors
				    when (string= pvs-type-name (get-type-id (range(type accessor))))
				    collect (kki-name-for-name-expr accessor)))))
    (if acycl-accessors
	(list (format nil "~a /* Acyclic ~a accessors */ := no (^(~{~a~^+ ~}) & iden)"
		      (kki-form-name)
		      pvs-type-name
		      acycl-accessors))
      nil)))

;;TODO remove once decided if selection axioms are not needed
;;
;; (defun get-sel-axioms (kki-set-for-adt constructors recognizers)
;;   (loop for ( constructor . accessors ) in constructors
;; 	for recognizer in recognizers
;; 	when (not (constant-contructor? constructor))
;; 	append (loop for accessor in accessors
;; 		     collect (format nil "~a /* Sel */ := ~a"
;; 				     (kki-form-name)
;; 				     (get-sel-axiom kki-set-for-adt accessor recognizer)))))
;;
;; (defun get-sel-axiom (kki-set-for-adt accessor recognizer)
;;   #+pvsdebug (format t "~%[get-sel-axiom] kki-set-for-adt ~a accessor ~a recognizer ~a ~%"
;; 		     kki-set-for-adt accessor recognizer)
;;   #+pvsdebug (format t "~&[get-sel-axiom] (domain (type accessor)) ~a ~%"
;; 		     (domain (type accessor)))
;;   (let ((quantifier  (format nil "all [S0 : one ~a]" kki-set-for-adt))
;; 	(inner-expr  (format nil "if (S0 in ~a) then (one S0.~a) else (no S0.~a)" 
;; 			     (let ((accesor-domain-type (domain (type accessor))))
;; 			       (kki-name-for-type-expr accesor-domain-type))
;; 			     (kki-name-for-name-expr accessor)
;; 			     (kki-name-for-name-expr accessor)))) 
;;     #+pvsdebug (format t "~&[get-sel-axiom] inner-expr ~a ~%"
;; 		       inner-expr)
;;     (format nil "~a | ~a" quantifier inner-expr)))

(defun get-uniqueness-axioms (constructors)
	(loop for ( constructor . accessors ) in constructors
	      collect (format nil "~a /* Uniqueness for ~a */ := ~a"
			      (kki-form-name) (id constructor) (get-uniqueness-axiom  constructor accessors))))

;; TODO remove
;; (defun kk-formula--constant-multiplicity (name-expr)
;;   "Generates the kodkod formula stating the multiplicity of the Kodkod element corresponding to NAME-EXPR, assuming it's a constant in the PVS specification."
;;   (if (is-atomic-constant? name-expr)
;;       (format nil "one ~a" (rest (get-relation-tuple-by-pvs-name (string (id name-expr)))))
;;     (error "Oops... incomplete!")))

;; The name of this function is somehow misleading.
;; If CONSTRUCTOR is constant, it produces a formula to state
;; that the corresponding kodkod set is an atom.
;; If it is not a constant, the function produces a formula
;; stating that each accessor is functional.
(defun get-uniqueness-axiom (constructor accessors)
  #+pvsdebug (format t "~%[get-uniqueness-axiom] constructor ~a accessors: ~{~a ~} ~%" (show constructor) accessors)
  (if (is-atomic-constant? constructor)
      (format nil "one ~a" (rest (get-relation-tuple-by-pvs-name (string (id constructor)))))
    (let*((kki-domain-sets (loop for accessor in accessors
				 collect (get-kki-name-for-pvs-type (range (type accessor)))))
	  (kki-ops      (loop for accessor in accessors
			      collect (kki-name-for-name-expr accessor)))
	  (vars         (loop for i from 0 to (- (length kki-domain-sets) 1)
			      collect (format nil "S~a" i)))
	  (bindings     (loop for kki-set-name in kki-domain-sets
			      for var in vars
			      collect (format nil "~a: one ~a" var kki-set-name)))
	  (inner-expr   (format nil "~{lone (~a)~^ && ~}" 
				(loop for kki-op in kki-ops
				      for var in vars
				      collect (format nil "~a.~a" var kki-op))))) 
      (format nil "all[~{~a~^, ~}] | ~a " bindings inner-expr ))))

(defun is-atomic-constant? (name-expr)
  (not (funtype? (type name-expr))))

(defun get-relation-tuple-by-pvs-name (rel-name)
	(assoc rel-name *used-relations* :test #'string=))

(defun translate-to-kki (expr &optional bindings)
	(translate-to-kki* expr bindings))

(defmethod translate-to-kki* ((expr quant-expr) bindings)
  #+pvsdebug (format t "~%[translate-to-kki* (quant-expr)] expr ~a bindings: ~{~a ~} ~%" expr bindings)
  (with-slots
   ((expr-bindings bindings) expression) expr
   (multiple-value-bind (newbindings bindvars)
       (translate-kodkodi-bindings expr-bindings bindings nil)
     #+pvsdebug (format t "~&[translate-to-kki* (quant-expr)] newbindings: ~{~a ~} ~%" newbindings)
     (let ((kki-body (translate-to-kki* expression newbindings)))
       (cond ((forall-expr? expr)
	      (format nil "all[~{~{~a: one ~a~}~^, ~}] | ~a"
		      bindvars kki-body))
	     ((exists-expr? expr)
	      (format nil "(some[~{~{~a: one ~a~}~^, ~}] | ~a)"
		      bindvars kki-body))))))) ;;no else case

(defun process-adt (datatype)
  #+pvsdebug (format t "~%[process-adt] datatype ~a~%" (show datatype))
  #+pvsdebug (format t "~&[process-adt] *used-adts* ~a~%" *used-adts*)
  #+pvsdebug (format t "~&[process-adt] (member datatype *used-adts* :key #'id) ~a~%" (member datatype *used-adts* :key #'id))
  (unless (member (id datatype) *used-adts* :key #'id)
    (push datatype *used-adts*)
    (loop for constructor in (constructors datatype)
	  do (kki-name-for-name-expr constructor))
    (loop for recognizer in (recognizers datatype)
	  do (kki-name-for-name-expr recognizer))
    (setq *kki-spec-formulas* (append (adt-kki-axioms datatype) *kki-spec-formulas*)))
  ;; #+pvsdebug (format t "~&[process-adt] *kki-spec-formulas* ~{~#[nil~:;~@{~a~^, ~}~]~:}~%" *kki-spec-formulas*)
  )

;; taken from metit.lisp:240
(defun translate-kodkodi-bindings (bind-decls bindings accum)
  #+pvsdebug (format t "~%[translate-kodkodi-bindings] bind-decls ~a bindings  ~{~a ~} accum  ~{~a ~} ~%" bind-decls bindings accum)
  (if (consp bind-decls)
      (let ((first-bind-decl (first bind-decls)))
	(let ((type (type first-bind-decl)))
	  (when (adt-type-name? type)
	    (process-adt type)))
	(if (not (type-name? (find-supertype (type first-bind-decl))));;(funtype? (type first-bind-decl))
	    (progn
	      (error 'unsopported-feature-error :text (format nil "Unsupported kind of type: ~a" (type first-bind-decl))))
	  (let*((yname     (kki-id-name ))
		(ytype
		 (get-kki-name-for-pvs-type (type first-bind-decl)))
		;; (yfullname (format nil "~a: one ~a" yname ytype)) ;;TODO remove
		(yfullname (list yname ytype)))
	    (translate-kodkodi-bindings (rest bind-decls) ;;making bindings here
					(cons 
					 (cons (string (get-type-id first-bind-decl)) yname)
					 bindings)
					(cons yfullname accum)))))
    (values bindings (nreverse accum))))

(defun get-forms-names ()
  (let ((names nil))
    (dotimes (n (+ 1 *kki-spec-formulas-count*))
      (setf names (cons (format nil "$f~a" n) names)))
    (rest (nreverse names))))

(defun kki-id-name ()
	(intern (format nil "S~a" (incf *var-count*))))

(defun kki-id-names (n)
 	(if (= n 0) nil
    	(cons (intern (format nil "s~a" (incf *unary-relation-count*)))
			(kki-id-names (1- n)))))

(defun kki-form-name ()
  (intern (format nil "$f~a" (incf *kki-spec-formulas-count*))))

;; el que llama a esta funcion tiene llamar con algo que no sea (pred)
;; ver op-kki-name

(defvar *debug-1* nil)

(define-condition unsopported-feature-error (error)
  ((text :initarg :text :reader text)))

(defun id-for-type (pvs-type)
  (cond ((setsubtype? pvs-type)
	 ;; setsubtypes do not have id
	 (intern (format nil "spuriousid~a" (sxhash (format nil "~a" pvs-type)))))
	((recognizer-name-expr? (predicate pvs-type))
	 (id (predicate pvs-type)))
	(t (error 'unsopported-feature-error
		  :text (format nil "Unsupported kind of type ~a" pvs-type)))))

(defun register-unary-type (pvs-type)
  #+pvsdebug (format t "~%[register-unary-type] pvs-type ~s ~%" pvs-type) ; debug
  (let*((type-id (get-type-id pvs-type)))
    #+pvsdebug (format t "~&[register-unary-type] type-id ~s ~%" type-id) ; debug
    (cond
     ((subtype? pvs-type)
      (let*((pvs-type-id   (id-for-type pvs-type))
	    (kki-name      (kki-name-for-type-expr pvs-type))
	    (tuple-set-reg (if (funtype? (type (predicate pvs-type)))
			       (get-kki-tuple-set-reg-for-pvs-type
				(domain (type (predicate pvs-type))))
			     (error 'unsopported-feature-error
				    :text (format nil "Unsupported kind of type: ~a" pvs-type)))))
	(when (setsubtype? pvs-type)
	  (let ((setsubtype-def (with-slots
				 ((expr-bindings bindings) expression) (predicate pvs-type)
				 (multiple-value-bind (newbindings bindvars)
				     (translate-kodkodi-bindings expr-bindings nil nil)
				   (let ((var-name ;; there can be just one variable, since this is a setsubtype
					  (first (first bindvars)))
					 (var-type
					  (second (first bindvars)))
					 (kki-body (translate-to-kki* expression newbindings)))
				     (format nil "all[~a: one ~a] | ~a in ~a <=> ~a"
					     var-name
					     var-type
					     var-name
					     kki-name
					     kki-body))))))
	    (push
	     (format nil "~a /* setsubtype definition */ := ~a" (kki-form-name) setsubtype-def)
	     *kki-spec-formulas*))
	  #+pvsdebug (format t "~&[register-unary-type] pvs-type ~s ~%" pvs-type)
	;; TODO remove
	  )
	;; TODO remove
	;; (setf *used-types*
	;;       (acons (or type-id (string pvs-type-id))
	;; 	     (cons kki-name tuple-set-reg)
	;; 	     *used-types*))
	(pushnew
	 (cons (or (string type-id) (string pvs-type-id)) (cons kki-name tuple-set-reg))
	 *used-types*
	 :test #'string=
	 :key #'car)
;;	#+pvsdebug (break "~&[register-unary-type] (subtype? pvs-type) t ~%")
	(car *used-types*)))
     (t
      (when type-id
	(let((kki-name      (format nil "s~a" (incf *unary-relation-count*)))
	     (tuple-set-reg (format nil "$a~a" (incf *tuple-set-reg-counter*))))
	  (if (subtype? pvs-type)
	      (let*((top-type (top-type pvs-type))
		    (top-type-kki-name (get-kki-name-for-pvs-type top-type)))
		(setq tuple-set-reg (get-kki-tuple-set-reg-for-pvs-type top-type)) 
		;; Generate the bounds spec corresponding to the pvs type
		(push (format nil
			      "bounds ~a: [~a, ~a]"
			      kki-name
			      (if *exact-bounds?* tuple-set-reg "{}")
			      tuple-set-reg)
		      ;; (format nil (if *exact-bounds?*
		      ;; 		      "bounds ~a: ~a"
		      ;; 		    "bounds ~a: [{}, ~a]")
		      ;; 	      kki-name tuple-set-reg)
		      *kki-spec-bounds*)
		;; Generate inclusion axiom
		(push (format nil "~a /* Inclusion axiom for type ~a */ := ~a in ~a"
			      (kki-form-name)
			      pvs-type
			      kki-name
			      top-type-kki-name)
		      *kki-spec-formulas*))
	    ;; Generate the bounds spec corresponding to the pvs type
	    (push (format nil "bounds ~a: [~a, ~a]"
			  kki-name
			  (if *exact-bounds?* tuple-set-reg "{}")
			  tuple-set-reg)
		  ;; (format nil (if *exact-bounds?*
		  ;; 		      "bounds ~a: ~a"
		  ;; 		    "bounds ~a: [{}, ~a]") kki-name tuple-set-reg)
		  *kki-spec-bounds*))
;;;;    
;;;; deprecated? 
	  (if (subtype? pvs-type)
	      (setf *subtypes-with-top-types*
		    (cons 
		     (cons (string type-id) (string (id (top-type pvs-type))))
		     *subtypes-with-top-types*))
	    (setf *pvs-top-types* (cons pvs-type *pvs-top-types*)))
;;;; deprecated?
;;;;
	  (setf *debug-1* pvs-type)
	  (setf *used-types*
		(acons (string type-id)
		       (cons kki-name tuple-set-reg)
		       *used-types*))
;;	  #+pvsdebug (break "~&[register-unary-type] (subtype? pvs-type) nil ~%")
	  (car *used-types*)))))))

(defun get-kki-tuple-set-reg-for-pvs-type (pvs-type)
  (let*((kki-data
	 (assoc (get-type-id pvs-type) *used-types* :test #'string=))
	(kki-data (if kki-data kki-data (register-unary-type pvs-type))))
    (cddr kki-data)))

;; pvs-type should be unary
(defun get-kki-name-for-pvs-type (pvs-type)
  (let*((kki-data
	 (assoc (get-type-id pvs-type) *used-types* :test #'string=))
	(kki-data (if kki-data kki-data (register-unary-type pvs-type))))
    (cadr kki-data)))

(defun interpreted-type? (typen)
  (member (string (get-type-id typen)) '(boolean) :test #'string=))

(defun get-type-id (var-type)
  (cond
   ((expr-as-type? var-type)
    (when (funtype? (type (expr var-type)))
	(get-type-id (domain (type (expr var-type))))))
   ((subtype? var-type)
    (let ((print-type (print-type var-type)))
      (if print-type
	  (format nil "~a" print-type)
	(id-for-type var-type))))
   ((set-type? var-type)
    (string (id (print-type var-type))))
   (t (format nil "~a~@[[~{~a~^,~}]~]" (id var-type) (actuals var-type)))))

(defun type-name-already-seen (ftype)	
	(assoc (string (get-type-id ftype)) *used-types* :test #'string=))

(defun is-pred? (type)
  (let*((list-of-types (types-of-fun type))
	(last-type     (car (last list-of-types))))
    (string= "boolean" (get-type-id last-type))))

(defun remove-last-if-boolean (list-of-types)
	(let ((last-type (car (last list-of-types))))
		(if (string= "boolean" (get-type-id last-type))
				(reverse (cdr (reverse list-of-types)))
			list-of-types)))
;;;; debug
(defmacro format-ret (spec data)
  `(progn (format t ,spec ,data)
	  ,data))

(defun kki-name-for-type-expr (type)
  (kki-name-for-id-and-type (id-for-type type) (if (subtype? type) (type (predicate type)) type)))

(defun kki-name-for-name-expr (expr)
  (let ((kki-name (kki-name-for-id-and-type (id expr) (type expr))))
;;    #+pvsdebug (format t "~%[kki-name-for-name-expr] kki-name ~a~%" kki-name)
    (let ((key kki-name)
	  (value (list (type expr) (string (id expr)))))
      (unless (assoc key *back-translation-data*)
	(setq *back-translation-data* (acons key value *back-translation-data*))))
;;    #+pvsdebug (format t "~&[kki-name-for-name-expr] *back-translation-data* ~a~%" *back-translation-data*)
    kki-name))

(defun kki-name-for-id-and-type (op-id op-type)
  #+pvsdebug (format t "~%[kki-name-for-id-and-type] op-id  ~a op-type ~a~%" op-id op-type)
  #+pvsdebug (when (eq 'e_ op-id) (break "[kki-name-for-id-and-type]" ))
  (unless (op-already-seen op-id) ;check if the operator was already defined
    (let*((original-types (types-of-fun op-type))
	  (op-type-as-list 
	   (remove-last-if-boolean original-types))
	  (is-pred (> (length original-types) (length op-type-as-list)))
	  (arity           (length op-type-as-list))
	  (relation-prefix (cond ((= 1 arity) "s")
				 ((= 2 arity) "r")
				 (t (format nil "m~a_" arity))))
	  (relation-name-index (cond ((= 1 arity) (incf *unary-relation-count*))
				     ((= 2 arity) (incf *binary-relation-count*))
				     (t           (incf *nary-relation-count*)))))
      (let ((op-kki-id (format nil "~a~a" relation-prefix relation-name-index))) 
	(setq *used-relations* (acons (string op-id) op-kki-id *used-relations*))
	;; Generate bounds axiom
	#+pvsdebug (format t "~%[kki-name-for-id-and-type] *exact-bounds?* ~a~%" *exact-bounds?*)
	(push (let((bound (format nil "~{~a~^->~}" (loop for type in op-type-as-list
							 collect
							 (get-kki-tuple-set-reg-for-pvs-type type)))))
		(format nil 
			"bounds ~a: [{},~a]"
			op-kki-id
			bound))
	      ;; (format nil 
	      ;; 	      (if *exact-bounds?*
	      ;; 		  "bounds ~a: ~{~a~^->~}"
	      ;; 		"bounds ~a: [{},~{~a~^->~}]")
	      ;; 	      op-kki-id
	      ;; 	      (loop for type in op-type-as-list
	      ;; 		    collect
	      ;; 		    (get-kki-tuple-set-reg-for-pvs-type type)))
	      *kki-spec-bounds*)
	;; Add domain axiom
	(push (format nil "~a /* ~a Domain */ := ~a in ~{~a~^->~} "
		      (kki-form-name)
		      op-id
		      op-kki-id
		      (loop for type in op-type-as-list collect (get-kki-name-for-pvs-type type)))
	      *kki-spec-formulas*)
	(when (not is-pred)
	  (push
	   (if (= 1 arity)
	       (format nil "~a /* ~a multiplicity */ := one ~a"
		       (kki-form-name) op-id op-kki-id)
	     (let*((domain-types (types(domain op-type)))
		   (vars (loop for i from 0 to (- (length domain-types) 1)
			       collect (format nil "S~a" i))) ;;; TODO this line assumes that every
		                                              ;;; type in domain-types is not functional/tuple-type
		   (kki-domain-sets (loop for type in domain-types
					  collect (get-kki-name-for-pvs-type type)))
		   (bindings     (loop for kki-set-name in kki-domain-sets
				       for var in vars
				       collect (format nil "~a: one ~a" var kki-set-name)))
		   (inner-expr (format nil "~a(~{~a~^, ~})" op-kki-id vars)))
	       (format nil "~a /* ~a multiplicity */ := all[~{~a~^, ~}] | lone ~a"
		 (kki-form-name)
		 op-id
		 bindings inner-expr)))
	   *kki-spec-formulas*)))))
  (rest (assoc (string op-id) *used-relations* :test #'string=)))

;;function to detect if a TYPE is a Set of another type (needs improvement probably)
(defun set-type? (typen)
	(funtype? typen))

(defun get-atoms-qty (univ-size)
	(floor (/ univ-size (length *pvs-top-types*))))

(defun get-top-type-id (var-type)
  (cond ((subtype? var-type)
	 (id (top-type var-type)))
	((set-type? var-type)
	 (id (print-type var-type)))
	(t (id var-type))))

(defun get-arity-types (rel-name)
  ;; uses the top-types, and not necessarily the actual types
  (let* ((types (rest (assoc (string rel-name)
			     *funtype-type-names-in-cartesian-product* :test #'string=)))
	 (real-domain (first types))
	 (real-range (second types))
	 (domain (string (get-top-type-id real-domain)))
	 (range (string (get-top-type-id real-range)))
	 (kki-domain (rest (assoc domain *used-types* :test #'string=)))
	 (kki-range (rest (assoc range *used-types* :test #'string=)))
	 (atom-domain (rest (assoc kki-domain *kki-type-atom-map* :test #'string=)))
	 (atom-range (rest (assoc kki-range *kki-type-atom-map* :test #'string=))))
    (format nil "~a -> ~a" atom-domain atom-range)))

(defun collect-atom-names-from-instance (instance)
  (loop for char across instance
	with atom = nil
	when (and atom (not (digit-char-p char))) collect atom and do (setq atom nil)
	when (and atom (digit-char-p char)) do (setq atom (format nil "~a~a" atom char))
	when (equal char #\A) do (setq atom "A")))


(defun parse-kki-instance (instance)
  (let ((result nil))
    ;; first, read the instance string to collect atoms by relation
    (loop for (kki-name . value) in *back-translation-data*
	for from  = (search kki-name instance)
	do (let*((from  (search kki-name instance))
		 (equal (search "=" instance :start2 from))
		 (empty (search "[]" instance :start2 equal)))
	     (unless (and empty (= empty (+ equal 1)))
	       (let*((to    (+ (search "]]" instance :start2 from) 1))
		     (line  (subseq instance from to))
		     (tuples-raw (make::split-string line :item #\]))
		     (tuples (loop for tuple in tuples-raw
				   for kki-atoms = (collect-atom-names-from-instance (format nil "~a]" tuple))
				   collect (loop for kki-atom in kki-atoms
						 ;; Add an ? to distinguish from PVS identifiers
						 collect (format nil "?~a" kki-atom))))
		     (atoms (collect-atom-names-from-instance line))
		     (ct-count (+ (count #\, line) 1)))
		 (setq result (acons kki-name (cons tuples value) result))))))
    #+pvsdebug (format t "~&[parse-kki-instance 1] result '~a'~%" result)
    ;; replace atoms by constant names
    (loop for (kki-name . value) in result
	  for tuples = (car value)
	  when (and (= 1 (length tuples)) (= 1 (length (car tuples))))
	  unless (is-pred? (second value))
	  do (let ((atom-to-replace (car (car tuples)))
		   (pvs-name (third value)))
	       (loop for (key . value) in result
		   do (setf (car value)
			    (loop for tuple in (car value)
				  collect (loop for atom in tuple
						when (string= atom atom-to-replace)
						collect pvs-name
						else
						collect atom)))))) 
    #+pvsdebug (format t "~&[parse-kki-instance 2] result '~a'~%" result)
    ;; replace atoms by function applications
    (loop for (kki-name . value) in result
	  for tuples = (car value)
	  when (and tuples (< 1 (length (car tuples))))
	  do (let(;; (atoms-to-replace (loop for tuple in tuples
		  ;; 			  append (last tuple)))
		  ;; (pvs-exprs        (loop for tuple in tuples
		  ;; 			  collect (format nil "~a(~{~a~^, ~})" (third value) (reverse (cdr (reverse tuple))))))
		  (atom-replacement (loop for tuple in tuples
					  when (equal (char (car(last tuple)) 0) #\?)
					  collect `( ,(car(last tuple)) .
						     ,(format nil "~a(~{~a~^, ~})" (third value) (reverse (cdr (reverse tuple))))))))
	       ;; #+pvsdebug (format t "~&[parse-kki-instance] atoms-to-replace(1) ~{ '~a'~}~%" atoms-to-replace)
	       ;; #+pvsdebug (format t "~&[parse-kki-instance] pvs-exprs(1) ~{ '~a'~}~%" pvs-exprs)
	       #+pvsdebug (format t "~&[parse-kki-instance] atom-replacement(1) '~a'~%" atom-replacement)
	       ;;
	       ;; primero hay que reemplazar en pvs-exprs misma
	       (loop for (atom . expr) in atom-replacement
		     do (loop for bind in atom-replacement
			      do (setf (cdr bind) (replace-all (cdr bind) atom expr))))
	       ;; #+pvsdebug (format t "~&[parse-kki-instance] pvs-exprs(2) ~{ '~a'~}~%" pvs-exprs)
	       #+pvsdebug (format t "~&[parse-kki-instance] atom-replacement(2) '~a'~%" atom-replacement)
	       (loop ;; for atom-to-replace in atoms-to-replace
		     ;; for pvs-expr in pvs-exprs
		     for (atom-to-replace . pvs-expr) in atom-replacement
		     when (equal (char atom-to-replace 0) #\?)
		     do (loop for (key . value) in result
			      do (setf (car value)
				       (loop for tuple in (car value)
					     collect (let((tuple-length (length tuple)))
						       (loop for atom in tuple
							     for i from 1
							     if (and t ;;(< i tuple-length)
							     	    ;;TODO sacar (string= atom atom-to-replace)
							     	    ) ;; no solo = ,inclusi'on
							     collect (replace-all atom atom-to-replace pvs-expr)
							     else
							     collect atom
							     ))))))))
    #+pvsdebug (format t "~&[parse-kki-instance 3] result '~a'~%" result)
    ;; build formulas
    (loop for (kki-name . (tuples pvs-type pvs-name)) in result
	  if (is-pred? pvs-type) 
	  append (loop for tuple in tuples 
		       collect (format nil 
				       "~a(~{~a~^, ~})" 
				       pvs-name tuple))
	  else
	  append (loop for tuple in tuples 
		       for lhs = (if (< 1 (length tuple))
				     (format nil 
					     "~a(~{~a~^, ~})" 
					     pvs-name (reverse(cdr(reverse tuple))))
				   pvs-name)
		       unless (string= lhs (car(last tuple)))
		       collect (format nil "~a = ~a" lhs (car(last tuple)))))))

;;TODO remove 
;; (defun kki-instance-to-pvs (instance)
;;   (let*((atom-pvs-ct nil)
;; 	(pvs-ct-decls
;; 	 (loop for (pvs-name . (kki-name . kki-tuple-reg)) in *used-types*
;; 	       for from  = (search kki-name instance)         
;; 	       for equal = (search "=" instance :start2 from) 
;; 	       for empty = (search "[]" instance :start2 equal)
;; 	       when (and (= (count #\( pvs-name) 0) ;; is not a pred type
;; 			 (not (and empty (= empty (+ equal 1)))))
;; 	       collect (let*((from  (search kki-name instance))
;; 			     (equal (search "=" instance :start2 from))
;; 			     (empty (search "[]" instance :start2 equal)))
;; 			 (unless (and empty (= empty (+ equal 1)))
;; 			   (let*((to    (+ (search "]]" instance :start2 from) 1))
;; 				 (line  (subseq instance from to))
;; 				 (atoms (collect-atom-names-from-instance line))
;; 				 (ct-count (+ (count #\, line) 1)))
;; 			     (format nil "~{ ~a~^, ~}: ~a"
;; 				     (loop for i from 0 to ct-count
;; 					   for atom-name in atoms
;; 					   for pvs-ct-nm = (format nil "~a_~a"
;; 					; (string-downcase (subseq pvs-name 0 1))
;; 								   pvs-name
;; 								   i)
;; 					   do (push (cons atom-name pvs-ct-nm) atom-pvs-ct)
;; 					   collect pvs-ct-nm)
;; 				     pvs-name))))))
;; 	(pvs-forms
;; 	 (loop for (pvs-name . kki-name) in *back-translation-data*
;; 	       for from  = (search kki-name instance)         
;; 	       for equal = (search "=" instance :start2 from) 
;; 	       for empty = (search "[]" instance :start2 equal)
;; 	       when (not (and empty (= empty (+ equal 1))))
;; 	       collect (let*((from (search kki-name instance))
;; 			     (equal (search "=" instance :start2 from))
;; 			     (empty (search "[]" instance :start2 equal)))
;; 			 (unless (and empty (= empty (+ equal 1)))
;; 			   (let*((to   (+ (search "]]" instance :start2 from) 1))
;; 				 (line (subseq instance from to))
;; 				 #+pvsdebug (dummy (format t "~%[kki-instance-to-pvs] line ~a~%" line))
;; 				 (tuples (make::split-string line :item #\]))
;; 				 #+pvsdebug (dummy (format t "~&[kki-instance-to-pvs] tuples ~a~%" tuples)))
;; 			     (format nil
;; 				     "~{ ~a~^~%~}~%"
;; 				     (loop for tuple in tuples
;; 					   for kki-atoms = (collect-atom-names-from-instance (format nil "~a]" tuple))
;; 					   for atoms = (loop for atom in kki-atoms
;; 							     collect (cdr (assoc atom atom-pvs-ct :test #'string=)))
;; ;;;;					   do (format t "~a ~a~%" tuple kki-atoms)
;; 					   if (> (length atoms) 1)
;; 					   collect (format nil "~a = ~a(~{~a~^, ~})"
;; 							   (car (last atoms))
;; 							   pvs-name
;; 							   (butlast atoms))
;; 					   else
;; 					   if (= (length atoms) 1)
;; 					   collect (format nil "~a = ~a"
;; 							   (car atoms)
;; 							   pvs-name)))))))))
;;     (if (or pvs-ct-decls pvs-forms)
;; 	(format nil "~{~a~%~}~%~{~a~%~}" pvs-ct-decls pvs-forms)
;;       (format nil "(Emtpy.)"))))

(defun translate-instance (instance)
  (loop for (pvs-name . (kki-name . kki-tuple-reg)) in *used-types*
	do (setf instance
		 (replace-all instance
			      (format nil "~a=" kki-name)
			      (format nil "~%~a = "(string pvs-name)))))
  (loop for (pvs-name . kki-name) in *used-relations*
	do (setf instance
		 (replace-all instance
			      (string kki-name)
			      (format nil "~%~a"(string pvs-name)))))
  ;; This is to remove the blank line
  (replace-all
   (replace-all instance
		(format nil "}~c~c" #\linefeed #\linefeed) "") "]]," "]]"))

(defun load-free-var-types (fvars names)
  (loop for fvar in fvars
	for name in names
	do (setf *free-var-types*
		 (cons (cons name (type (extra-get-expr fvar))) *free-var-types*))))

(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurences of the part is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
           for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                            :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos))) 

(defmethod translate-to-kki* ((expr name-expr) bindings)
  #+pvsdebug (format t "~%[translate-to-kki* (name-expr)] expr ~a bindings ~{~#[nil~:;~@{~a~^, ~}~]~:} ~%" expr bindings)
  #+pvsdebug (format t "~&[translate-to-kki* (name-expr)] (is-variable-expr expr) ~a ~%" (is-variable-expr expr))
  (let ((result
	 (or
	  ;; first, try to find the name in the bindings
	  (rest (assoc (id expr) bindings :test #'string=))
	  ;; then, check if it's a built-in symbol
	  (kki-interpretation expr)
	  ;; finally, look for previously seen symbols
	  (rest (assoc (format nil "~a" expr) *used-relations* :test #'string=)))))
    (if result result
      (let ((supertype (find-supertype(type expr))))
	(when (adt-type-name? supertype)
	  (process-adt supertype))
	(let ((result
	       (rest (assoc (format nil "~a" expr) *used-relations* :test #'string=))))
	  (if result result
	    ;; (error "~%[translate-to-kki* (name-expr)] Unrecognized name ~a.~%" expr)
	    ;; if the name is still missing, it should be the first apparition
	    ;; of an user-defined constant.
	    (kki-name-for-name-expr expr)))))))

; taken from metit.lisp:259
(defun kodkodi-interpretation (name-expr)
	(assert (name-expr? name-expr))
  (id name-expr))

(defmethod translate-to-kki* ((expr cases-expr) bindings)
  ;; (break "[translate-to-kki* (cases-expr)]")
  (error "~%Unsupported feature: cases-expr.~%"))

(defmethod translate-to-kki* ((expr mixfix-branch) bindings)
  (let ((guar-expr (args1 expr))
	(then-expr (args2 expr))
	(else-expr (third (arguments expr))))
      (format nil "(if ~a then ~a else ~a)"
	      (translate-to-kki* guar-expr bindings)
	      (translate-to-kki* then-expr bindings)
	      (translate-to-kki* else-expr bindings))))

;;TODO used? remove?
;; possible replacement
;; (collect-subterms expr #'(lambda (e) (and (application? e) (not (is-bool-type (type e))))))
(defmethod collect-non-bool-subexprs ((expr application))
  (if (is-bool-type (type expr))
      (let ((args  (if (arg-tuple-expr? (argument expr)) 
		       (exprs (argument expr))
		     (list  (argument expr)))))
	(loop for arg in args
	      append (collect-non-bool-subexprs arg)))
    (list expr)))

;;TODO used? remove?
(defmethod collect-non-bool-subexprs ((expr expr))
  (unless (is-bool-type (type expr)) (list expr)))

(defmethod translate-to-kki* ((expr application) bindings)
  #+pvsdebug (format t "~%[translate-to-kki* (application)] expr ~a bindings ~{~#[nil~:;~@{~a~^, ~}~]~:} ~%" expr bindings)
;;  #+pvsdebug (break "[translate-to-kki*]")
  (with-slots (operator argument) expr
    (if (and (name-expr? operator));check not to translate _pred	      
	(let* ((op-id (kki-interpretation operator))
	       (op-id (if op-id op-id operator)))
	  (case op-id
	    (~ (format nil "!~a" (translate-to-kki* (argument expr) bindings)))
	    (^ (format nil "~a~a~a"
		       (translate-to-kki* (args1 expr) bindings)
		       op-id
		       (translate-to-kki* (args2 expr) bindings)))
	    ((=)
	     (if (is-bool-type (type(args1 expr)))
		 (format nil "(~a <=> ~a)"
			 (translate-to-kki* (args1 expr) bindings)
			 (translate-to-kki* (args2 expr) bindings))
	       (format nil "(~a ~a ~a)" (translate-to-kki* (args1 expr) bindings)
		       op-id
		       (translate-to-kki* (args2 expr) bindings))))
	    ((=> <=> && \|\|)
	     (format nil "(~a ~a ~a)" (translate-to-kki* (args1 expr) bindings)
		     op-id
		     (translate-to-kki* (args2 expr) bindings)))
	    ((/=)                       ; since kodkodi has no "not equals to" operator,
					; "/=" is translated as the 
					; negation of the equality.
	     (format nil "!(~a=~a)" (translate-to-kki* (args1 expr) bindings)
		     (translate-to-kki* (args2 expr) bindings)))
	    (t (let ((args  (if (arg-tuple-expr? (argument expr)) 
				(exprs (argument expr))
			      (list  (argument expr)))))
		 (let*((op-type     (type(operator expr)))
		       (op-kki-name (kki-name-for-name-expr (operator expr))))
		   (if (is-pred? op-type)
		       (format nil "(~{~a~^->~}) in ~a" ;; "some ((~{~a~^->~}) & ~a)"
			       (loop for arg in args
				     collect (translate-to-kki* arg bindings))
			       op-kki-name)
		     (let(#+pvsdebug
			  (dummy
			   (format t "~&[translate-to-kki* (application)] args ~{~#[nil~:;~@{~a~^, ~}~]~:} ~%" args)))
		       (format nil "~a(~{~a~^,~})" 
			     op-kki-name
			     (loop for arg in args
				   collect (translate-to-kki* arg bindings))))))))))
      (error ":-O expression ~a cannot be handled." operator))))

(defun funtype-kki-sets-in-cartesian-product (op-kki-id ftype)
  (unless (string= (string (get-type-id (range ftype))) "boolean")
    (setf *funtype-type-names-in-cartesian-product* 
	  (acons 
	   (string op-kki-id)
	   (types-of-fun ftype)
	   *funtype-type-names-in-cartesian-product*))))

(defun op-already-seen (op-id)
	(assoc (string op-id) *used-relations* :test #'string=))

(defun types-of-fun (type)
	"If type is a funtype representing TYPE [ T1 -> ... -> Tn ], this function returns a iist containing the names of the types T1 .. Tn in that order, Otherwise it returns nil."
	(if (funtype? type)
	    (let ((domain (domain type))
		  (range  (range  type)))
	      (append 
	       (if (funtype? domain)
		   (types-of-fun domain)
		 (if (tupletype? domain)
		     (loop for dom-type in (types domain)
			   append (types-of-fun dom-type))
		   ;; let's assume it is a type-name
		   (list domain)))
	       (if (funtype? range)
		   (types-of-fun range)
		 (if (tupletype? range)
		     (loop for ran-type in (types range)
			   append (types-of-fun ran-type))
		   ;; let's assume it is a type-name
		   (list range)))))
	  (list type)))

(defun all-adts (theory)
  (loop for decl in (all-declarations theory) when (typep decl 'datatype) collect decl))

(defun all-decls-generated-by-adts (theory)
  (loop for adt in (all-adts theory) append (generated adt)))

(defun explicitly-defined-axioms (theory)
  (loop for decl in (set-difference (all-declarations theory) (all-decls-generated-by-adts theory))
	when (axiom? decl)
	collect decl))

(defun translate-axioms (theory)
  (loop for decl in (explicitly-defined-axioms theory) 
	collect `(,(if (member (id decl) *overriden-axioms*)
		       (format t "~&Warning: omitting axiom ~a (overriden)~%" (id decl))
		     (handler-case
			   (translate-to-kki (definition decl)) 
			 (unsopported-feature-error
			  (ex)
			  (format t "~&Warning: omitting axiom ~a (~a)~%" (id decl) (text ex)))))
		  . ,(id decl))))

(defmethod translate-to-kki* ((expr rational-expr) bindings)
  (if (number-expr? expr)
      (number expr)
    (let ((rat (number expr)))
      (format nil "(~a / ~a)" (numerator rat) (denominator rat)))))

(defparameter *kki-interpreted-names* 
  '((IMPLIES (|booleans| . =>))
    (=> (|booleans| . =>))
    (AND (|booleans| . &&))
    (& (|booleans| . &))
    (OR (|booleans| . \|\|)) 
    (NOT (|booleans| . ~))
    (IFF (|booleans| . <=>))
    (<=> (|booleans| . <=>))
    (TRUE (|booleans| . "true"))
    (FALSE (|booleans| . "false"))
    (= (|equalities| . =))
    (/= (|notequal| . /=))

    ;; (< (|reals| . <))
    ;; (<= (|reals| . <=))
    ;; (> (|reals| . >))
    ;; (>= (|reals| . >=))
    ;; (+  (|number_fields| . +))
    ;; (- (|number_fields| . -))
    ;; (* (|number_fields| .  *))
    ;; (/ (|number_fields| . /))
    ;; (^ (|exponentiation| . ^))
    ;; (sin (|sincos_def| . sin) (|trig_basic| . sin)) 
    ;; (cos (|sincos_def| . cos) (|trig_basic| . cos))
    ;; (tan (|sincos_def| . tan) (|trig_basic| . tan))
    ;; (pi (|atan| . pi) (|trig_basic| . pi))
    ;; (e  (|ln_exp| . "exp(1)"))
    ;; (asin (|asin| . arcsin))
    ;; (atan (|atan| . arctan))
    ;; (acos (|acos| . arccos))
    ;; (sqrt (|sqrt| . sqrt))
    ;; (sq (|sq| . sq))
    ;; (ln (|ln_exp| . ln))
    ;; (exp (|ln_exp| . exp))
    ;; (sinh (|hyperbolic| . sinh))
    ;; (cosh (|hyperbolic| . cosh))
    ;; (tanh (|hyperbolic| . tanh))
    ;; (abs (|real_defs| . abs))
    ;; (\#\# (|interval| . \#\#))
    
    ))

(defun kki-interpretation (name-expr)
  (assert (name-expr? name-expr))
  (let* (#+pvsdebug (dummy (format t "~%[kki-interpretation] name-expr ~a ~%" name-expr))
	 (id-assoc (rest (assoc (id name-expr) *kki-interpreted-names*)))
	 #+pvsdebug (dummy (format t "~&[kki-interpretation] id-assoc ~a ~%" id-assoc))
	 (mod-assoc (rest (assoc (id (module-instance
				      (resolution name-expr)))
				 id-assoc)))
	 #+pvsdebug (dummy (format t "~&[kki-interpretation] mod-assoc ~a ~%" mod-assoc))
;;	 #+pvsdebug (dummy (break "kki-interpretation"))
	 )
    mod-assoc))



;;;; debug

;; (defmethod proofstepper ((proofstate proofstate))

;; ;;The key part of the proofstate for the stepper is the strategy field.
;; ;;It indicates which rule to apply to the current goal, how to proceed
;; ;;with the subgoals generated, and how to deal with failures.
;; ;;We have to be careful to ensure that the strategies do not meddle with
;; ;;logical things, i.e., they merely indicate which rules are applied
;; ;;where.  So, it might be better if the strategy merely indicated the
;; ;;top-level rule and the subgoal strategies.
;; ;;a rule-application yields a signal to pop with failure, pop with
;; ;;success, no change (if the rule wasn't applicable), or in the most
;; ;;usual case, a list of subgoals.  To achieve some measure of type
;; ;;correctness, we have a class of rules, and rule application is a
;; ;;method for this class.
;; ;;The defn. below is tentative.  It needs to be cleaned up later.

;; ;;(NSH:4-10-91) I want to use strategies in two ways.  One is as a
;; ;;strategy for applying patterns of rules, and the other is as a rule
;; ;;itself.  The first one is invoked as change-strategy and the second
;; ;;one as apply-strategy.  The second one generates lifts all the pending
;; ;;subgoals back to the current proofstate and thus behaves as a derived
;; ;;rule.  It also won't print out any output.  In replaying a proof, the
;; ;;apply-strategies will be replayed but the change-strategies will not.
;;   ;;
;;   ;; (format t "~%[proofstepper] proofstate {- ~s -}~%" proofstate) ;;debug
;;   (cond
;;     ((fresh? proofstate)   ;;new state
;;      (let ((post-proofstate ;;check if current goal is prop-axiom.
;; 	    (cond ((eq (check-prop-axiom (s-forms (current-goal proofstate)))
;; 		       '!) ;;set flag to proved! and update fields.
;; 		   (pvs-json:update-ps-control-info-result proofstate) ; M3 so the sequent
;; 					; it's accumulated for the rpc response.
;; 					; It could be a call to output-proofstate
;; 					; but currently that would disturb the
;; 					; behavior of the wish viewer.
;; 		   (setf (status-flag proofstate) '!      
;; 			 (current-rule proofstate) '(propax)
;; 			 (printout proofstate)
;; 			 (format nil "~%which is trivially true.")
;; 			 (justification proofstate)
;; 			 (make-instance 'justification
;; 			   :label (label-suffix (label proofstate))
;; 			   :rule '(propax)))
;; 		   proofstate)	    ;;else display goal, 
;; 		  ;;eval strategy, invoke rule-apply
;; 		  (t (catch-restore ;;in case of restore/enable interrupts
;; 		      (progn
;; 			(when ;;(not (strat-proofstate? proofstate))
;; 			    ;;NSH(8.19.95): display-proofstate called only
;; 			    ;;when current state is root, or has rule/input.
;; 			    ;;in-apply taken care of in display-proofstate. 
;; 			    (and (not (strat-proofstate? proofstate))
;; 				 (or (null (parent-proofstate proofstate))
;; 				     (current-rule (parent-proofstate proofstate))
;; 				     (current-input (parent-proofstate proofstate))))
;; 			  (apply-proofstate-hooks proofstate))
;; 			(let* ((*rule-args-alist* nil)
;; 			       (strategy
;; 				(strat-eval*
;; 				 (if (strategy proofstate)
;; 				     (strategy proofstate)
;; 				     '(postpone t)) ;;if no strat, move on.
;; 				 proofstate))
;; 			       (*proof-strategy-stack*
;; 				(cons strategy *proof-strategy-stack*)))
;; 			  (setf (strategy proofstate) strategy)
;; 			  (rule-apply strategy proofstate))))))))
;;        ;;rule-apply returns a revised proofstate.
;;        (cond ((null post-proofstate) ;;hence aborted
;; 	      (let ((nps	     ;;NSH(7.18.94) for proper restore
;; 		     (nonstrat-parent-proofstate
;; 		      proofstate)))
;; 		(setf (status-flag nps) nil
;; 		      (remaining-subgoals nps) nil
;; 		      (current-subgoal nps) nil
;; 		      (pending-subgoals nps) nil
;; 		      (done-subgoals nps) nil
;; 		      (strategy nps)
;; 		      (query*-step)) ;;unevaluated
;; 		;;(query*-step)
;; 		;;is okay here.
;; 		nps))
;; 	     ((eq (status-flag post-proofstate) '?)	 ;;subgoals created
;; 	      (format-printout post-proofstate)		 ;;print commentary
;; 	      (cond ((> (length (remaining-subgoals post-proofstate)) 1)
;; 		     (when (and *rerunning-proof*
;; 				(integerp *rerunning-proof-message-time*)
;; 				(> (- (get-internal-real-time)
;; 				      *rerunning-proof-message-time*)
;; 				   3000)) ;;print mini-buffer msg
;; 		       (setq *rerunning-proof* (format nil "~a." *rerunning-proof*))
;; 		       (setq *rerunning-proof-message-time*
;; 			     (get-internal-real-time))
;; 		       (pvs-message *rerunning-proof*))
;; 		     (format-nif "~%this yields  ~a subgoals: "
;; 				 (length (remaining-subgoals post-proofstate))))
;; 		    ((not (typep (car (remaining-subgoals post-proofstate))
;; 				 'strat-proofstate))
;; 		     (format-nif "~%this simplifies to: ")))
;; 	      post-proofstate)
;; 	     ((eq (status-flag post-proofstate) '!) ;;rule-apply proved
;; 	      ;; M3: call hooks for success [Sept 2020]
;; 	      (dolist (hook *success-proofstate-hooks*)
;; 		(funcall hook proofstate)) 
;; 	      (format-printout post-proofstate)
;; 	      (wish-done-proof post-proofstate)
;; 	      (dpi-end post-proofstate)
;; 					;		       (when (printout post-proofstate)
;; 					;			 (format-if (printout post-proofstate)))
;; 	      post-proofstate)
;; 	     (t  post-proofstate))))
;;     ;;if incoming goal has subgoals
;;     ((eq (status-flag proofstate) '?) ;;working on subgoals
;;      (cond ((null (remaining-subgoals proofstate))
;; 	    (cond ((null (pending-subgoals proofstate))
;; 		   (success-step proofstate)) ;;no more subgoals,declare success
;; 		  (t			      ;;pending subgoals
;; 		   (post-processing-step proofstate))))
;; 	   (t ;;subgoals remain
;; 	    (let ((newps (pop (remaining-subgoals proofstate))))
;; 	      (setf ;;(parent-proofstate newps) proofstate
;; 	       (current-subgoal proofstate) newps)
;; 	      ;; (substitution newps)
;; 	      ;; (if (null (out-substitution proofstate))
;; 	      ;;     (substitution proofstate)
;; 	      ;;     (out-substitution proofstate))
;; 	      ;; (context newps)
;; 	      ;; (if (null (out-context proofstate))
;; 	      ;;     (context proofstate)
;; 	      ;;     (out-context proofstate))
	      
;; 	      ;; (when (eq (status-flag newps) '*)
;; 	      ;;   (if (null (remaining-subgoals newps));;leaf node
;; 	      ;;       (setf (status-flag newps) nil;;make it fresh
;; 	      ;;             (strategy newps)
;; 	      ;;             (strategy proofstate))
;; 	      ;;             (post-subgoal-strat (strategy proofstate))
;; 	      ;;             nil
;; 	      ;;       (setf (status-flag newps) '?
;; 	      ;;       (strategy newps)
;; 	      ;;       (strategy proofstate))
;; 	      ;;       (post-subgoal-strat (strategy proofstate))
;; 	      ;;       nil
;; 	      ;;   )
;; 	      ;;   (setf (strategy proofstate);;zero out strategy
;; 	      ;;         (if (null (remaining-subgoals proofstate))
;; 	      ;;             nil
;; 	      ;;             (strategy proofstate))))
;; 	      newps))))
;;    ((eq (status-flag proofstate) '*)  ;;pending goals, but proceeding
;;     (next-proofstate proofstate))
;;    ((memq (status-flag proofstate) '(X XX))  ;;failure
;;     (format-if "~%~%Attempted proof of ~a failed." (label proofstate))
;;     (next-proofstate proofstate))
;;    ((eq (status-flag proofstate) '!)  ;;success
;;     ;;(format t "~%~%Proved ~a." (label proofstate))
;;     (next-proofstate proofstate))
;;    (t (next-proofstate proofstate))))

;; (defun rule-apply (step ps)
;;   ;;   (format t "~%[rule-apply] step ~a ps {- ~s -} ~%" step (format nil "~s" ps))
;;   ;;   (format t "~%[rule-apply] (typep step 'rule-instance) ~a ~%" (typep step 'rule-instance))
;;   ;;   (format t "~%[rule-apply] (typep (topstep step) 'rule-instance) ~a ~%" (typep (topstep step) 'rule-instance))
;;   ;;   (format t "~%[rule-apply] (typep (topstep step) 'strategy) ~a ~%" (typep (topstep step) 'strategy))
;;   (let* ((*ps* ps)
;; 	 (* '*) ;; Causes problems when debugging, but needed to interpret :fnums *
;; 	 (*goal* (current-goal ps))
;; 	 (*label* (label  ps))
;; 	 ;; (*subgoalnum* (subgoalnum ps))
;; 	 (*+* (mapcar #'formula (pos-s-forms (current-goal ps))))
;; 	 (*-* (mapcar #'formula (neg-s-forms (current-goal ps))))
;; 	 (*par-label* (when (parent-proofstate ps)
;; 			(label (parent-proofstate ps))))
;; 	 (*par-goal* (when (parent-proofstate ps)
;; 		       (current-goal (parent-proofstate ps))))
;; 	 (*current-context* *current-context*)
;; 	 (*module-context* (copy-prover-context))
;; 	 (*auto-rewrites* (rewrites ps))
;; 	 (*all-rewrites-names* (all-rewrites-names ps))
;; 	 (*auto-rewrites-names* (auto-rewrites-names ps))
;; 	 (*auto-rewrites!-names* (auto-rewrites!-names ps))
;;  	 (*macro-names* (macro-names ps))	 
;; 	 (*rewrite-hash* (rewrite-hash ps))
;; 	 (*dp-state* (dp-state ps)))
;;     (cond ((typep step 'rule-instance);;if step is a rule, then
;; 	   ;;reinvoke rule-apply with corresponding strategy. 
;; 	   (rule-apply (make-instance 'strategy
;; 				      :topstep step)
;; 		       ps));;else step is a strategy
;; 	  ((typep (topstep step) 'rule-instance)
;; 	   (let* ((*tccforms* nil)
;; 		  (topstep (topstep step))
;; 		  (name (if (consp (rule-input topstep))
;; 			    (car (rule-input topstep))
;; 			  topstep))
;; 		  (*suppress-printing* (or *suppress-printing*
;; 					   (eq name 'lisp))))
;; 	     (when (memq name *ruletrace*)
;; 	       (format t "~%~vTEnter: ~a" *ruletracedepth*
;; 		       (rule-input topstep))
;; 	       (incf *ruletracedepth*))
;; 	     (multiple-value-bind (signal subgoals updates)
;; 		 (funcall (rule topstep) ps) ;;(break "rule-ap")
;; 	       (cond ((eq signal '!)	     ;;success
;; 		      (when (memq name *ruletrace*)
;; 			(decf *ruletracedepth*)
;; 			(format t "~%~vT Exit: ~a -- Proved subgoal"
;; 				*ruletracedepth* name ))
;; 		      (when (eq (car (rule-input topstep)) 'apply)
;; 			(let ((timeout-sect
;; 			       (memq :timeout (rule-input topstep))))
;; 			  (when timeout-sect
;; 			    (setf (rule-input topstep)
;; 				  (append (ldiff (rule-input topstep)
;; 						 timeout-sect)
;; 					  (cddr timeout-sect))))))
;; 		      (setf (status-flag ps) '!      
;; 			    (current-rule ps) (rule-input topstep)
;; 			    (parsed-input ps) (sublis (remove-if #'null
;; 								 *rule-args-alist*
;; 								 :key #'cdr)
;; 						      (rule-input topstep))
;; 			    (printout ps) (sublis (remove-if #'null
;; 							     *rule-args-alist*
;; 							     :key #'cdr)
;; 						  (rule-format topstep))
;; 			    (justification ps) (make-instance 'justification
;; 							      :label (label-suffix (label ps))
;; 							      :rule (rule-input topstep)
;; 							      :comment (new-comment ps)))
;; 		      (make-updates updates ps)
;; 		      ps)
;; 		     ((eq signal '?) ;;subgoals generated
;; 		      (make-updates updates ps)
;; 		      ;;(NSH:5/1/99)make-updates should be above
;; 		      ;;make-subgoal-proofstates
;; 		      ;;so that the proof-dependent-decls can be computed.
;; 		      (let* ((*tccforms* (remove-duplicates *tccforms*
;; 							    :test #'tc-eq
;; 							    :key #'tccinfo-formula))
;; 			     (tcc-hash-counter 0)
;; 			     (*tccforms*
;; 			      (loop for tcc in *tccforms*
;; 				    when (or
;; 					  (null (gethash
;; 						 (tccinfo-formula tcc)
;; 						 (tcc-hash ps)))
;; 					  (and (incf tcc-hash-counter)
;; 					       nil))
;; 				    collect tcc))
;; 			     (new-tcc-hash
;; 			      (if *tccforms*
;; 				  (copy (tcc-hash ps))
;; 				(tcc-hash ps)))
;; 			     (tccforms (assert-tccforms *tccforms* ps))
;; 			     (false-tccforms
;; 			      (loop for tccform in *tccforms*
;; 				    as assert-tccform in tccforms
;; 				    when (tc-eq (tccinfo-formula assert-tccform)
;; 						*false*)
;; 				    collect tccform)))
;; 			(cond (false-tccforms
;; 			       ;;treated as a skip
;; 			       (unless *suppress-printing*
;; 				 (format t "~%No change. False TCCs: ~%~{  ~a, ~%~}"
;; 					 (mapcar #'tccinfo-formula false-tccforms)))
;; 			       (setf (status-flag ps) nil ;;start afresh
;; 				     (strategy ps)
;; 				     (failure-strategy step))
;; 			       ps)
;; 			      (t (let* ((tcc-subgoals
;; 					 (make-tcc-subgoals tccforms ps))
;; 					(subgoal-proofstates
;; 					 (make-subgoal-proofstates
;; 					  ps
;; 					  (subgoal-strategy step)
;; 					  subgoals
;; 					  tcc-subgoals
;; 					  ;;updates;must be attached to subgoals.
;; 					  ))
;; 					(all-subgoals (append subgoals tcc-subgoals)))
;; 				   (let((analysis-results
;; 					 (and (not *noninteractive*)
;; 					      *model-finder-validation*
;; 					      (memq name '(case)) ;; list of commands to analyze
;; 					      ;; alternative: 
;; 					      ;; (< 1 (length all-subgoals)) ;; validate only when the proof splits
;; 					      (analyze-subgoals (current-goal ps) all-subgoals)
;; 					      )))
;; 				     (progn
;; 				       (when (and analysis-results (null (car analysis-results)))
;; 					 (format-if "Warning: Model for the antecedents in ~a.1 cannot be found. The sequent might be inconsistent.~%" (label ps)))
;; 				       (if ;; (some  (lambda (result) (and (stringp result)
;; 					   ;; 				(string= "Counterexample found" result)))
;; 					   ;; 	  (cdr analysis-results))
;; 					   ;; in panic-mode, just the last one is worth to be checked
;; 					   (string= "Counterexample found" (car (last analysis-results)))
;; 					   (progn
;; 					     ;; (error-format-if "subgoals validation failed: ~{~a~}."
;; 					     ;; 		      (cdr analysis-results))
;; 					     (error-format-if "subgoals validation failed: counterexample found for sequent ~a.~a." (label ps) (length analysis-results))
;; 					     (setf (status-flag ps) nil ;;start afresh
;; 						   (strategy ps)
;; 						   (failure-strategy step))
;; 					     ps)
;; 					 ;;cleaning up (NSH 7.27.94)
;; 					 ;;1. convert main subgoals of tccs into
;; 					 ;;non-tccs.
;; 					 ;;2. hash the new tccs into new-tcc-hash
;; 					 ;;3. set tcc-hash of main subgoals as
;; 					 ;;new-tcc-hash
;; 					 (progn
;; 					   (when (> tcc-hash-counter 0)
;; 					     (format-if "~%Ignoring ~a repeated TCCs."
;; 							tcc-hash-counter))
;; 					   (loop for tcc in *tccforms*
;; 						 do
;; 						 (setf (gethash (tccinfo-formula tcc)
;; 								new-tcc-hash)
;; 						       t))
;; 					   (assert
;; 					    (every #'(lambda (sps)
;; 						       (every #'(lambda (sfmla)
;; 								  (null (freevars (formula sfmla)))
;; 								  )
;; 							      (s-forms (current-goal sps))))
;; 						   subgoal-proofstates))
;; 					   (loop for sps in subgoal-proofstates
;; 						 when (not (tcc-proofstate? sps))
;; 						 do (setf (tcc-hash sps)
;; 							  new-tcc-hash))
;; 					   (when (memq name *ruletrace*)
;; 					     (decf *ruletracedepth*)
;; 					     (format t "~%~vT Exit: ~a -- ~a subgoal(s) generated."
;; 						     *ruletracedepth* name (length subgoal-proofstates)))
;; 					   (push-references *tccforms* ps)
;; 					   (when (eq (car (rule-input topstep)) 'apply)
;; 					     (let* ((rinput (rule-input topstep))
;; 						    (mtimeout (memq :timeout rinput)))
;; 					       (if mtimeout
;; 						   ;; Given by keyword
;; 						   (setf (rule-input topstep)
;; 							 (append (ldiff rinput mtimeout)
;; 								 (cddr mtimeout)))
;; 						 ;; Positional
;; 						 (when (and (not (some #'keywordp rinput))
;; 							    (> (length rinput) 4))
;; 						   (assert (= (length rinput) 5))
;; 						   (setf (rule-input topstep)
;; 							 (butlast rinput))))))
;; 					   (setf (status-flag ps) '?
;; 						 (current-rule ps) (rule-input topstep)
;; 						 (parsed-input ps) (sublis (remove-if #'null
;; 										      *rule-args-alist*
;; 										      :key #'cdr)
;; 									   (rule-input topstep))
;; 						 (printout ps) (sublis (remove-if #'null
;; 										  *rule-args-alist*
;; 										  :key #'cdr)
;; 								       (rule-format topstep))
;; 						 (remaining-subgoals ps) subgoal-proofstates)
;; 					   (unless (typep ps 'top-proofstate)
;; 					     (setf (strategy ps) nil))
;; 					   ps)))))))))
;; 		     ((eq signal 'X)
;; 		      (when (memq name *ruletrace*)
;; 			(decf *ruletracedepth*)
;; 			(format t "~%~vT Exit: ~a -- No change."
;; 				*ruletracedepth* name))
;; 		      (unless *suppress-printing*
;; 			(explain-errors)
;; 			(commentary "~%No change on: ~s" (rule-input topstep)))
;; 		      (setf (status-flag ps) nil ;;start afresh
;; 			    (strategy ps)
;; 			    (failure-strategy step))
;; 		      (make-updates updates ps)
;; 		      ps)
;; 		     ((eq signal 'XX) ;;marks the current goal a failure
;; 		      (setf (status-flag ps) 'XX)
;; 		      ps)
;; 		     ((eq signal '*)
;; 		      (setf (status-flag ps) '*)
;; 		      ps)
;; 		     (t (undo-proof signal ps))))))
;; 	  ((typep (topstep step) 'strategy)
;; 	   (setf (status-flag ps) '?
;; 		 ;;		 (current-rule ps) (strategy-input rule)
;; 		 (remaining-subgoals ps)
;; 		 (make-subgoal-proofstates ps
;; 					   (topstep step)
;; 					   (list (current-goal ps))))
;; 					;nil
;; 	   ps)
;; 	  (t (format t "~%Bad rule: ~a~%" step);;treated as skip
;; 					;(break)
;; 	     (setf (status-flag ps) nil;;start afresh
;; 		   (strategy ps)
;; 		   (failure-strategy (strategy ps)))
;; 	     ps))))
